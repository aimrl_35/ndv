﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.MvcEdition;

namespace MapSuite_using_MVC.Models
{
    public class helping
    {
        int prov_ID;
        int dist_ID;
        int teh_ID;
        String attr_Name;
        string teh_name;
        int attr_ID;
        int gender;
        int subAttr;
        Map pakMap;
        public int Prov_ID { get; set; }
        public int Dist_ID { get; set; }
        public int Teh_ID { get; set; }
        public string Teh_Name { get; set; }
        public string Attr_Name { get; set; }

        public int Attr_ID { get; set; }
        public int Gender { get; set; }
        public int SubAttr { get; set; }
        public Map PakMap { get; set; }
    }
}