//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MapSuite_using_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class District
    {
        public int District_ID { get; set; }
        public string District_Name { get; set; }
        public string Occupied_Area { get; set; }
        public Nullable<int> Province_ID { get; set; }
    
        public virtual Province Province { get; set; }
    }
}
