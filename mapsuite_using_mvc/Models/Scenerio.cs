//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MapSuite_using_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Scenerio
    {
        public Scenerio()
        {
            this.Multi_Value = new HashSet<Multi_Value>();
            this.New_People_Data = new HashSet<New_People_Data>();
            this.New_Place_Data = new HashSet<New_Place_Data>();
        }
    
        public int Scenerio_ID { get; set; }
        public Nullable<int> Location_ID { get; set; }
        public string Age { get; set; }
        public string Gender { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> Attribute_ID { get; set; }
    
        public virtual ICollection<Multi_Value> Multi_Value { get; set; }
        public virtual ICollection<New_People_Data> New_People_Data { get; set; }
        public virtual ICollection<New_Place_Data> New_Place_Data { get; set; }
    }
}
