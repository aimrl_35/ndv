//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MapSuite_using_MVC.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Attribute
    {
        public Attribute()
        {
            this.Sub_Attribute = new HashSet<Sub_Attribute>();
        }
    
        public int Attribute_ID { get; set; }
        public string Attribute_Name { get; set; }
        public Nullable<int> Flag { get; set; }
    
        public virtual ICollection<Sub_Attribute> Sub_Attribute { get; set; }
    }
}
