﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.MvcEdition;
using MapSuite_using_MVC.Models;

namespace MapSuite_using_MVC.Controllers
{
    public class MapController : Controller
    {
        //
        // GET: /Map/

        public ActionResult Index()
        {
            return View();
        }
        Map Map2 = null;
        public ActionResult NDVMap()
        {
            Map2 = new Map("map",
       new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage), 510);
            //new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage);
            //WorldMapKitWmsWebOverlay worldMapKitOverlay = new WorldMapKitWmsWebOverlay("WorldMapKitOverlay");
            //Map2.CustomOverlays.Add(worldMapKitOverlay);



            Map2.MapTools.LoadingImage.ImageUri = new Uri(Server.MapPath(@"~/ShapeFiles/loading_logofinal_by_zegerdon-d60eb1v.gif"));
            Map2.MapTools.LoadingImage.Enabled = true;
            Map2.MapTools.LoadingImage.Height = 64;
            Map2.MapTools.LoadingImage.Width = 64;
            Map2.MapTools.ScaleLine.Enabled = true;
            Map2.MapUnit = GeographyUnit.DecimalDegree;
            Map2.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LongitudeLatitude;
            Map2.MapTools.MouseCoordinate.Enabled = true;
            Map2.Cursor = CursorType.Default;
          

            Map2.MapBackground.BackgroundBrush = new GeoSolidBrush(GeoColor.StandardColors.SlateGray);

          
            ShapeFileFeatureLayer PakLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));
            PakLayer.ZoomLevelSet.ZoomLevel04.DefaultAreaStyle = AreaStyles.Country1;
            PakLayer.ZoomLevelSet.ZoomLevel04.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Crimson, 3, 3);//TextStyles.Capital3("NAME_1");
            PakLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level06;
            ShapeFileFeatureLayer.BuildIndexFile(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));


            
            LayerOverlay staticOverlay = new LayerOverlay("StaticOverlay");
            staticOverlay.IsBaseOverlay = false;
            staticOverlay.Layers.Add("PakLayer", PakLayer);
            

            PakLayer.Open();
            Map2.CurrentExtent = PakLayer.GetBoundingBox();
            Map2.CustomOverlays.Add(staticOverlay);



            //InMemoryFeatureLayer highlightLayer = new InMemoryFeatureLayer();
            //highlightLayer.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(165, 42, 42, 100), GeoColor.GeographicColors.DeepOcean);
            //highlightLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            
            //LayerOverlay dynamicOverlay = new LayerOverlay("HightLightDynamicOverlay");
            //dynamicOverlay.Layers.Add("HighLightLayer", highlightLayer);
            
            //dynamicOverlay.IsBaseOverlay = false;
            //Map2.CustomOverlays.Add(dynamicOverlay);
            
            Map2.Popups.Add(new CloudPopup("information", Map2.CurrentExtent.GetCenterPoint()) { AutoSize = true, IsVisible = false });
            ViewBag.map2 = Map2;
            Session["map"] = Map2;
            return View();
        }


        [MapActionFilter]
        public int changeLoction(Map map, GeoCollection<object> args)
        {
            try { 
            LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            if (args[0].ToString() == "Pakistan")
            {
                staticOverlay.Layers.RemoveAt(0);
                ShapeFileFeatureLayer pakLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));
                pakLayer.ZoomLevelSet.ZoomLevel04.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.Silver, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange));
                pakLayer.ZoomLevelSet.ZoomLevel04.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3);//TextStyles.Capital3("NAME_1");
                pakLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level07;
                staticOverlay.Layers.Add(pakLayer);
            }
            if (args[0].ToString() == "Provinces") 
            {
                staticOverlay.Layers.RemoveAt(0);
                ShapeFileFeatureLayer provLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"));
                provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PaleGreen, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_1", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3));//TextStyles.Capital3("NAME_1");
                provLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level09;
                staticOverlay.Layers.Add(provLayer);
            }
            if (args[0].ToString() == "Districts")
            {
                staticOverlay.Layers.RemoveAt(0);
                ShapeFileFeatureLayer distLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
                distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add (AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.LightYellow, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_3", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3));//TextStyles.Capital3("NAME_1");
                distLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                staticOverlay.Layers.Add(distLayer);
            }
            if (args[0].ToString() == "Tehsils")
            {
                staticOverlay.Layers.RemoveAt(0);
                ShapeFileFeatureLayer tahseelLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_Tehsil_Boundary.shp"));
                tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("TEHSIL", "Arial", 6, DrawingFontStyles.Italic, GeoColor.StandardColors.Black, 3, 3));
                tahseelLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PastelOrange, GeoColor.FromArgb(100, GeoColor.SimpleColors.Black)));
                staticOverlay.Layers.Add(tahseelLayer);
            }
            }catch(Exception ex){
                return 0;
            }
            return 1;
        }
        [MapActionFilter]
        public void changeAttribute(Map map, GeoCollection<object> args)
        {
            if (args[0].ToString() == "Pakistan")
            {

            }
            if (args[0].ToString() == "Provinces")
            {
                List<String> provArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm1";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                   provArray.Add(row["NAME_1"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_1";
                for (int i = 0; i < provArray.Count; i++)
                {
                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }
            if (args[0].ToString() == "Districts")
            {
                List<String> distArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm3";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    distArray.Add(row["NAME_3"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);
                
                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_3";
                for (int i = 0; i < distArray.Count; i++)
                {
                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }
            if (args[0].ToString() == "Tehsils")
            {

                List<String> tehsilArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from Pak_Tehsil_Boundary";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/Pak_Tehsil_Boundary.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    tehsilArray.Add(row["TEHSIL"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);
                
                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "TEHSIL";
             
                for (int i = 0; i < tehsilArray.Count; i++)
                {
                   
                    valueStyle.ValueItems.Add(new ValueItem(tehsilArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i+1)/2]))));

                }

                

                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }

        }

        [MapActionFilter]//for index page map
        public void showDataOnMap(Map map, GeoCollection<object>args)
        {
            var loc = args[0].ToString();
            var attr = args[1].ToString();
            var sub_attr_name = args[3].ToString();
           
            try
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                if (args[0].ToString() == "Pakistan")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer pakLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles2/PAK_adm0.shp"));
                    pakLayer.ZoomLevelSet.ZoomLevel04.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.Silver, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange));
                    pakLayer.ZoomLevelSet.ZoomLevel04.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3);//TextStyles.Capital3("NAME_1");
                    pakLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level07;
                    staticOverlay.Layers.Add(pakLayer);
                }
                if (args[0].ToString() == "Provinces")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer provLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles2/PAK_adm1.shp"));
                    provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PaleGreen, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                    provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_1", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));//TextStyles.Capital3("NAME_1");
                    provLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level09;
                    staticOverlay.Layers.Add("CapitalLayer",provLayer);
                }
                if (args[0].ToString() == "Districts")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer distLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles2/PAK_adm3.shp"));
                    distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.LightYellow, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                    distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_3", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));//TextStyles.Capital3("NAME_1");
                    distLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                    staticOverlay.Layers.Add("DistrictLayer",distLayer);
                }
                if (args[0].ToString() == "Tehsils")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer tahseelLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles2/PAK_Tehsil_Boundary.shp"));
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("TEHSIL", "Arial", 6, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PastelOrange, GeoColor.FromArgb(100, GeoColor.SimpleColors.Black)));
                    staticOverlay.Layers.Add("TahseelLayer",tahseelLayer);
                }
            }
            catch (Exception ex)
            {

            }
            

            if (args[0].ToString() == "Pakistan")
            {

            }
            if (args[0].ToString() == "Provinces")
            {
                LayerOverlay staticOverlay2 = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                List<String> provArray = new List<String>();
                staticOverlay2.Layers.Remove("PakLayer");
                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm1";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles2/PAK_adm1.shp"),ShapeFileReadWriteMode.ReadWrite);
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    provArray.Add(row["NAME_1"].ToString());
                }
                LayerOverlay staticOvrlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOvrlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_1";
                //getting data of all prov
             
                for (int i = 0; i < provArray.Count; i++)
                {
                    
                    //here will be logic of coloring
                    Object[] data = OnlyProvinceWithAttribute(provArray[i], attr);
                    
                    if (data == null)
                    {
                        valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Black))));
                    }
                    else 
                    {
                        string []name=(string[])data[0];
                        List<long> values=(  List<long>)data[1];
                        var percentAge=0.0;
                        for (int s = 0; s < name.Length;s++)
                        {
                            if (name[s] == args[3].ToString())
                            {
                                percentAge = (values[s] * 100) / values.Sum();
                                if (percentAge >= 1 && percentAge <= 10)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Red))));
                                    break;
                                }
                                if (percentAge >= 11 && percentAge <= 20)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Green))));
                                    break;
                                }
                                if (percentAge >= 21 && percentAge <= 30)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue))));
                                    break;
                                }
                                if (percentAge >= 31 && percentAge <= 40)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Orange))));
                                    break;
                                }
                                if (percentAge >= 41 && percentAge <= 50)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Yellow))));
                                    break;
                                }
                                if (percentAge >= 51 && percentAge <= 60)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Violet))));
                                    break;
                                }
                                if (percentAge >= 61 && percentAge <= 70)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.SkyBlue))));
                                    break;
                                }
                                if (percentAge >= 71 && percentAge <= 80)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Silver))));
                                    break;
                                }
                                if (percentAge >= 81 && percentAge <= 90)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Pink))));
                                    break;
                                }
                                if (percentAge >= 91 && percentAge <= 100)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Brown))));
                                    break;
                                }
                               
                            }
                        }
                            //valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));
                    }
                }

                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
              //  map.CurrentExtent = shapeFileLayer.GetBoundingBox();
            }
            if (args[0].ToString() == "Districts")
            {
                List<String> distArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm3 where NAME_1='Punjab'AND NAME_2='Lahore'";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles2/PAK_adm3.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    distArray.Add(row["NAME_3"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_3";
                for (int i = 0; i < distArray.Count; i++)
                {
                    //here will be logic of coloring
                    Object[] data = OnlyProvinceAndDistrictWithSub_Attribute(distArray[i],args[1].ToString(),int.Parse(args[2].ToString()));

                    if (data == null)
                    {
                        valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Black))));
                    }
                    else
                    {
                        string[] name = (string[])data[1];
                        List<long> values = (List<long>)data[2];
                        var percentAge = 0.0;
                        for (int s = 0; s < name.Length; s++)
                        {
                            if (name[s] == args[3].ToString())
                            {
                                percentAge = (values[s] * 100) / values.Sum();
                                if (percentAge >= 1 && percentAge <= 10)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Red))));
                                    break;
                                }
                                if (percentAge >= 11 && percentAge <= 20)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Green))));
                                    break;
                                }
                                if (percentAge >= 21 && percentAge <= 30)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue))));
                                    break;
                                }
                                if (percentAge >= 31 && percentAge <= 40)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Orange))));
                                    break;
                                }
                                if (percentAge >= 41 && percentAge <= 50)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Yellow))));
                                    break;
                                }
                                if (percentAge >= 51 && percentAge <= 60)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Violet))));
                                    break;
                                }
                                if (percentAge >= 61 && percentAge <= 70)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.SkyBlue))));
                                    break;
                                }
                                if (percentAge >= 71 && percentAge <= 80)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Silver))));
                                    break;
                                }
                                if (percentAge >= 81 && percentAge <= 90)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Pink))));
                                    break;
                                }
                                if (percentAge >= 91 && percentAge <= 100)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Brown))));
                                    break;
                                }

                            }
                        }
                        //valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));
                    }

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
                shapeFileLayer.Open();
                map.CurrentExtent = shapeFileLayer.GetBoundingBox();
                shapeFileLayer.Close();
            }
            if (args[0].ToString() == "Tehsils")
            {

                List<String> tehsilArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from Pak_Tehsil_Boundary";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles2/Pak_Tehsil_Boundary.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    tehsilArray.Add(row["TEHSIL"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "TEHSIL";

                for (int i = 0; i < tehsilArray.Count; i++)
                {

                    valueStyle.ValueItems.Add(new ValueItem(tehsilArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }

        }
        [MapActionFilter]
        public void showDataOnMap2(Map map, GeoCollection<object> args)
        {
            var loc = args[0].ToString();
            var attr = args[1].ToString();
            var sub_attr_name = args[3].ToString();

            try
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                if (args[0].ToString() == "Pakistan")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer pakLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));
                    pakLayer.ZoomLevelSet.ZoomLevel04.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.Silver, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange));
                    pakLayer.ZoomLevelSet.ZoomLevel04.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3);//TextStyles.Capital3("NAME_1");
                    pakLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level07;
                    staticOverlay.Layers.Add(pakLayer);
                }
                if (args[0].ToString() == "Provinces")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer provLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"));
                    provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PaleGreen, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                    provLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_1", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));//TextStyles.Capital3("NAME_1");
                    provLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level09;
                    staticOverlay.Layers.Add(provLayer);
                }
                if (args[0].ToString() == "Districts")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer distLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
                    distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.LightYellow, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange)));
                    distLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("NAME_3", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));//TextStyles.Capital3("NAME_1");
                    distLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                    staticOverlay.Layers.Add(distLayer);
                }
                if (args[0].ToString() == "Tehsils")
                {
                    staticOverlay.Layers.RemoveAt(0);
                    ShapeFileFeatureLayer tahseelLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_Tehsil_Boundary.shp"));
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("TEHSIL", "Arial", 6, DrawingFontStyles.Italic, GeoColor.StandardColors.White, 3, 3));
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level14;
                    tahseelLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PastelOrange, GeoColor.FromArgb(100, GeoColor.SimpleColors.Black)));
                    staticOverlay.Layers.Add(tahseelLayer);
                }
            }
            catch (Exception ex)
            {

            }


            if (args[0].ToString() == "Pakistan")
            {

            }
            if (args[0].ToString() == "Provinces")
            {
                List<String> provArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm1";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"), ShapeFileReadWriteMode.ReadWrite);
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    provArray.Add(row["NAME_1"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_1";
                //getting data of all prov

                for (int i = 0; i < provArray.Count; i++)
                {

                    //here will be logic of coloring
                    Object[] data = OnlyProvinceWithAttribute(provArray[i], attr);

                    if (data == null)
                    {
                        valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Black))));
                    }
                    else
                    {
                        string[] name = (string[])data[0];
                        List<long> values = (List<long>)data[1];
                        var percentAge = 0.0;
                        for (int s = 0; s < name.Length; s++)
                        {
                            if (name[s] == args[3].ToString())
                            {
                                percentAge = (values[s] * 100) / values.Sum();
                                if (percentAge >= 1 && percentAge <= 10)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Red))));
                                    break;
                                }
                                if (percentAge >= 11 && percentAge <= 20)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Green))));
                                    break;
                                }
                                if (percentAge >= 21 && percentAge <= 30)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Blue))));
                                    break;
                                }
                                if (percentAge >= 31 && percentAge <= 40)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Orange))));
                                    break;
                                }
                                if (percentAge >= 41 && percentAge <= 50)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Yellow))));
                                    break;
                                }
                                if (percentAge >= 51 && percentAge <= 60)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Violet))));
                                    break;
                                }
                                if (percentAge >= 61 && percentAge <= 70)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.SkyBlue))));
                                    break;
                                }
                                if (percentAge >= 71 && percentAge <= 80)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Silver))));
                                    break;
                                }
                                if (percentAge >= 81 && percentAge <= 90)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Pink))));
                                    break;
                                }
                                if (percentAge >= 91 && percentAge <= 100)
                                {
                                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(GeoColor.StandardColors.Brown))));
                                    break;
                                }

                            }
                        }
                        //valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));
                    }
                }

                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }
            if (args[0].ToString() == "Districts")
            {
                List<String> distArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from PAK_adm3";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    distArray.Add(row["NAME_3"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_3";
                for (int i = 0; i < distArray.Count; i++)
                {
                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }
            if (args[0].ToString() == "Tehsils")
            {

                List<String> tehsilArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select * from Pak_Tehsil_Boundary";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/Pak_Tehsil_Boundary.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    tehsilArray.Add(row["TEHSIL"].ToString());
                }
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers[0]);

                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, 255);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "TEHSIL";

                for (int i = 0; i < tehsilArray.Count; i++)
                {

                    valueStyle.ValueItems.Add(new ValueItem(tehsilArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[(i + 1) / 2]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel04.CustomStyles.Add(valueStyle);
            }

        }
        [HttpPost]
        public JsonResult OnlyProvinceAndDistrictWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column values
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var element = location_List.ElementAt(i);

                var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                           select zz.Scenerio_ID).ToList();

                for (int j = 0; j < sc1.Count; j++)     // Multi Values
                {
                    var element1 = sc1.ElementAt(j);
                    var Records = (from x in db.Multi_Value
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records);
                }
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }
            long sum = 0;
            List<long> l2 = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                for (int k = n; k < l.Count; k = k + colName.Length)
                {
                    sum += l.ElementAt(k);
                }
                l2.Add(sum);
            }

            return this.Json(l2, JsonRequestBehavior.AllowGet);
        }

       // [HttpPost]
        public Object[] OnlyProvinceWithAttribute(string province, string att)
        {
            List<long> SumOfValues = new List<long>();
            Object[] NamesAndValues;
            try {
                if (province == "Punjab")
                {
                    NDVEntities db = new NDVEntities();
                    DbfToNDV loc = db.DbfToNDVs.First(n=>(n.Dbf_Loc_Name==province));
                    Models.Province prov = db.Provinces.First(x => (x.Province_Name == loc.NDV_Loc_Name));//here comes province
                    var location_List = (from yy in db.Locations
                                         where (yy.Province_ID == prov.Province_ID)
                                         select yy.Location_ID).ToList();

                    Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == att));

                    var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                                   where x.Attribute_ID == attr.Attribute_ID
                                   select x.SubAttr_Name).ToArray();

                    List<Array> AllRecords = new List<Array>();
                    for (int i = 0; i < location_List.Count; i++)
                    {
                        var Location_Ids = location_List.ElementAt(i); //20

                        var Scenerio_List = (from zz in db.Scenerios        // 13
                                             where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                             select zz.Scenerio_ID).ToList();

                        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                        {
                            var element1 = Scenerio_List.ElementAt(j);
                            var Records = (from x in db.Multi_Value     // 13
                                           where x.Scenerio_ID == element1
                                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                            AllRecords.Add(Records); // ( 13 * 4 ) * 20
                        }
                    }

                    List<long> ListOfMultiValues = new List<long>();

                    for (int m = 0; m < AllRecords.Count; m++)
                    {
                        Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                        for (int k = 0; k < Scenerio_Record.Length; k++)
                        {
                            ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                        }
                    }

                 
                    for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
                    {
                        long sum = 0;
                        for (int k = n; k < ListOfMultiValues.Count; k = k + colName.Length)
                        {
                            sum += ListOfMultiValues.ElementAt(k);
                        }
                        SumOfValues.Add(sum);   // alag alag values
                    }

                    
                    NamesAndValues = new Object[2];

                    NamesAndValues[0] = colName;        // Names
                    NamesAndValues[1] = SumOfValues;    // Values

                 //   return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
                }
                else 
                { 
                    SumOfValues = null;
                    NamesAndValues = null;
                }
                }
            catch(Exception e)
            {
                SumOfValues = null;
                NamesAndValues = null;
            }
            
            return NamesAndValues;

        }

        // [HttpPost]
        public long OnlyProvinceWithSub_Attribute(string province,string att,int sub_att)
        {
            NDVEntities db = new NDVEntities();
            Models.Province prov = db.Provinces.First(x => (x.Province_Name == province));//here comes province
            var location_List = (from yy in db.Locations
                                 where (yy.Province_ID == prov.Province_ID)
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == att));
            Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == sub_att));

            var cols = (from x in db.Sub_Attribute
                        where x.Attribute_ID == attr.Attribute_ID
                        select x.SubAttr_Name).ToArray();

            var colName = from x in db.Sub_Attribute
                          where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == sub_att)
                          select x.SubAttr_Name;
            long sum = 0;
            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            // sub ki id
            for (int k = sub_att - 1; k < ListOfMultiValues.Count; k = k + cols.Length)
            {
                sum += ListOfMultiValues.ElementAt(k);
            }
            // alag alag values

            return sum;
        }

       // [HttpPost]
        //public Object[] OnlyProvinceAndDistrictWithSub_Attribute(string dis, string att, int sub_att)
        //{
        //    Object[] NamesAndValues;
        //    DbfToNDV loc=null;
        //    NamesAndValues = new Object[2];
            
        //    NDVEntities db = new NDVEntities();
        //    try { 
        //    loc = db.DbfToNDVs.First(n=>(n.Dbf_Loc_Name==dis));
        //         }
        //    catch(Exception e)
        //    {
        //    loc =null;
        //    }
        //    if (loc != null)
        //    {
        //        var location_List = (from yy in db.Locations
        //                             where ((yy.District_ID == loc.NDV_ID))
        //                             select yy.Location_ID).ToList();
        //        if(location_List.Count!=0)
        //        {
        //            try { 
        //        Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == att));
        //        Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == sub_att));

        //        var colName = (from x in db.Sub_Attribute
        //                       where x.Attribute_ID == attr.Attribute_ID
        //                       select x.SubAttr_Name).ToArray();

        //        var SubAttName = from x in db.Sub_Attribute
        //                         where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == sub_att)
        //                         select x.SubAttr_Name;
        //        long sum = 0;
        //        List<Array> AllRecords = new List<Array>();
        //        for (int i = 0; i < location_List.Count; i++)
        //        {
        //            var Location_Ids = location_List.ElementAt(i); //20

        //            var Scenerio_List = (from zz in db.Scenerios        // 13
        //                                 where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
        //                                 select zz.Scenerio_ID).ToList();

        //            for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //            {
        //                var element1 = Scenerio_List.ElementAt(j);
        //                var Records = (from x in db.Multi_Value     // 13
        //                               where (x.Scenerio_ID == element1)
        //                               select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //                AllRecords.Add(Records); // ( 13 * 4 ) * 20
        //            }
        //        }

        //        List<long> ListOfMultiValues = new List<long>();

        //        for (int m = 0; m < AllRecords.Count; m++)
        //        {
        //            Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
        //            for (int k = 0; k < Scenerio_Record.Length; k++)
        //            {
        //                ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
        //            }
        //        }

        //        // sub ki id
        //        for (int k = sub_att - 1; k < ListOfMultiValues.Count; k = k + colName.Length)
        //        {
        //            sum += ListOfMultiValues.ElementAt(k);
        //        }



        //        if (sum != 0)
        //        {
        //            NamesAndValues[0] = SubAttName;     // Names
        //            NamesAndValues[1] = sum;            // Values
        //        }
        //        else
        //        {
        //            NamesAndValues = null;
        //        }
                
        //                }
        //            catch(Exception e)
        //            {
        //                NamesAndValues = null;
        //                }
        //        }
        //        else
        //        {
        //            NamesAndValues = null;
        //        }
        //    }
        //    else
        //    {
        //        NamesAndValues = null;
        //    }
        //    return NamesAndValues;
        //}

        public Object[] OnlyProvinceAndDistrictWithSub_Attribute(string dis, string att, int sub_att)
        {
            NDVEntities db = new NDVEntities();
             Object[] NamesAndValues=null;
            Models.DbfToNDV dist = null;
            try
            {
               dist = db.DbfToNDVs.First(x => (x.Dbf_Loc_Name == dis));//here comes province
            }
            catch (Exception e)
            {
                dist = null;
            }
            if (dist != null)
            {
                var location_List = (from yy in db.Locations
                                     where ((yy.District_ID == dist.NDV_ID))//hard-coded
                                     select yy.Location_ID).ToList();
                if (location_List.Count >= 1&&dist.NDV_Loc_Name!=null) { 
                Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == att));

                var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                               where x.Attribute_ID == attr.Attribute_ID
                               select x.SubAttr_Name).ToArray();

                List<Array> AllRecords = new List<Array>();
                for (int i = 0; i < location_List.Count; i++)
                {
                    var element = location_List.ElementAt(i);

                    var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                               where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                               select zz.Scenerio_ID).ToList();

                    for (int j = 0; j < sc1.Count; j++)     // Multi Values
                    {
                        var element1 = sc1.ElementAt(j);
                        var Records = (from x in db.Multi_Value
                                       where x.Scenerio_ID == element1
                                       select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                        AllRecords.Add(Records);
                    }
                }

                List<long> l = new List<long>();

                for (int m = 0; m < AllRecords.Count; m++)
                {
                    Array fir = AllRecords.ElementAt(m);
                    for (int k = 0; k < fir.Length; k++)
                    {
                        l.Add((long)fir.GetValue(k));   // List of all values
                    }
                }

                List<long> SumOfValues = new List<long>();
                for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
                {
                    long sum = 0;
                    for (int k = n; k < l.Count; k = k + colName.Length)
                    {
                        sum += l.ElementAt(k);
                    }
                    SumOfValues.Add(sum);
                }
                String[] Years;
                Years = new String[3];
                Years[0] = "2007";
                Years[1] = "2008";
                Years[2] = "2009";

                NamesAndValues = new Object[3];
                NamesAndValues[0] = Years;        // Names
                NamesAndValues[1] = colName;        // Names
                NamesAndValues[2] = SumOfValues;    // Values
                }
                else
                {
                    NamesAndValues = null;

                }
            }
            return NamesAndValues;
        }
        [HttpPost]
        public JsonResult attToSubAttr(MapSuite_using_MVC.Models.Attribute a)
        {
            //int id = int.Parse(tid);
            NDVEntities db = new NDVEntities();
            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == a.Attribute_Name));

            var disList1 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_Name).ToArray();

            var disList2 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }
    }
}
