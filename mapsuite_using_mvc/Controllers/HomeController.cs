﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MapSuite_using_MVC.Models;
using System.Web.Mvc.Properties;
using System.IO;

using System.Web.UI;
using System.Web.UI.WebControls;

using ThinkGeo.MapSuite.MvcEdition;
using ThinkGeo.MapSuite.Core;
using System.Collections.ObjectModel;

using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using System.Data;
using System.Data.OleDb;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.Entity.Infrastructure;

namespace MapSuite_using_MVC.Controllers
{
    public class HomeController : Controller
    {
        public int uid = 0;

        //----------------Data Entery through excel file----
        //
        // GET: /DataEntry/

        public ActionResult AttachFile()
        {
            return View();
        } 
        private bool isValidExtention(string contentType)
        {
            if (contentType.Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") || contentType.Equals("application/vnd.ms-excel"))
                return true;
            else
                return false;
        }

        //validating the data in sr,gender,year, feilds 
        //need to validate age group
        private bool isvalid(string fileaddress)
        {
            Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
            //This is my test excel file;
            Microsoft.Office.Interop.Excel.Workbook exceldoc = application.Workbooks.Open(fileaddress);

            Worksheet exlwooksheet = (Microsoft.Office.Interop.Excel.Worksheet)exceldoc.Sheets[1];
            Excel.Range xlRange = exlwooksheet.UsedRange;

            object[,] valueArray = (object[,])xlRange.get_Value(
                        Excel.XlRangeValueDataType.xlRangeValueDefault);

            int number;
            var LastRow = exlwooksheet.UsedRange.Rows.Count;
            LastRow = LastRow + exlwooksheet.UsedRange.Row - 1;
            string sr = "sr";
            string year = "year", gender = "gender", agegroup = "age group";
            string male = "male", female = "female";
            int i = 0;
            for (i = 1; i <= LastRow; i++)
            {
                var x = valueArray[1, 1].ToString().ToLower();
                x = valueArray[1, 2].ToString().ToLower();
                x = valueArray[1, 3].ToString().ToLower();
                x = valueArray[1, 4].ToString().ToLower();

                if (i == 1)
                {
                    if (sr == valueArray[1, 1].ToString().ToLower() & year == valueArray[1, 2].ToString().ToLower() &
                        gender == valueArray[1, 3].ToString().ToLower() & agegroup == valueArray[1, 4].ToString().ToLower())
                    {

                    }
                    else { exceldoc.Close(); return false; }
                }
                else
                {
                    if (int.TryParse(valueArray[i, 1].ToString(), out number) &
                       (int.TryParse(valueArray[i, 2].ToString(), out number) & int.Parse(valueArray[i, 2].ToString()) >= 1947 & int.Parse(valueArray[i, 2].ToString()) <= System.DateTime.Now.Year) &
                        (male == valueArray[i, 3].ToString().ToLower() | female == valueArray[i, 3].ToString().ToLower()))
                    {
                        for (var col = 5; col < exlwooksheet.UsedRange.Columns.Count; col++)
                        {
                            if (int.TryParse(valueArray[i, col].ToString(), out number))
                            {

                            }
                            else
                            {
                                exceldoc.Close();
                                return false;
                            }
                        }
                    }
                    else { exceldoc.Close(); return false; }

                }
            }

            exceldoc.Close();
            return true;
        }

        //receiving file
        [HttpPost]
        public ActionResult process(HttpPostedFileBase photo)
        {
            var validExtention = isValidExtention(photo.ContentType);

            if (validExtention)
            {
                ViewBag.extention = photo.ContentType;

                var fileName = Path.GetFileName(photo.FileName);
                var path = Path.Combine(Server.MapPath("~/deletefiles"), fileName);
                var orignalpath = Path.Combine(Server.MapPath("~/file"), fileName);
                // photo.SaveAs(orignalpath);
                photo.SaveAs(path);

                if (isvalid(path))
                {
                    ViewBag.success = "Succesfully uploaded";
                    photo.SaveAs(orignalpath);
                }
                else
                    ViewBag.fail = "Don't use special character / , ? . ect";
            }
            else
            {
                ViewBag.error = "Invalid Entention olny excel file";
            }
            return View("AttachFile");
        }

        public ActionResult generateFile(string y)
        {
            NDVEntities db = new NDVEntities();

            int id = (int)(from x in db.Attributes
                           where x.Attribute_Name.Equals(y)
                           select x.Attribute_ID).First();

            var colNames = (from x in db.Sub_Attribute
                            where x.Attribute_ID == id
                            select x.SubAttr_Name).ToList();

            colNames.Insert(0, "sr");
            colNames.Insert(1, "Year");
            colNames.Insert(2, "Gender");
            colNames.Insert(3, "Age Group");
            string file = @"E:\Internship\NDV version 3 29oct\NDV\NDV\file\template.xls";


            Random rand = new Random();

            Microsoft.Office.Interop.Excel.Application application = new Microsoft.Office.Interop.Excel.Application();
            //This is my test excel file;
            Microsoft.Office.Interop.Excel.Workbook exceldoc = application.Workbooks.Open(file);

            Worksheet exlwooksheet = (Microsoft.Office.Interop.Excel.Worksheet)exceldoc.Sheets[1];

            var LastRow = exlwooksheet.UsedRange.Rows.Count;
            LastRow = LastRow + exlwooksheet.UsedRange.Row - 1;



            for (int row = 1; row <= exlwooksheet.UsedRange.Rows.Count; row++)
            {
                for (int col = 1; col <= exlwooksheet.UsedRange.Columns.Count; col++)
                {
                    Excel.Range range = exlwooksheet.get_Range("A1", "A6000");
                    Excel.Range roww = range.EntireRow;
                    roww.Delete();
                    //Microsoft.Office.Interop.Excel.Range cel = (Range) application.Cells[1, col];
                    //cel.Delete();
                }
            }

            //exceldoc.Save();

            //exlwooksheet.Cells[1, 1] = "sr#";
            //exlwooksheet.Cells[1, 2] = "Year";
            //exlwooksheet.Cells[1, 3] = "Gender";
            //exlwooksheet.Cells[1, 4] = "Age Group";

            int count = 1;
            foreach (var x in colNames)
            {
                exlwooksheet.Cells[1, count] = x;
                count++;
            }

            string[] agegroup = new string[] { "2", "18 plus", "15 - 19", "20 - 24", "25 - 29", "30 - 34", "35 - 39", "40 - 44", "45 - 49", "50 - 54", "55 - 59", "60 - 64", "65 - 69", "70 - 74", "75 & ABOVE" };

            // Excel.Worksheet mvshet = (Excel.Worksheet)mvbk.Sheets[1]; // Explicit cast is not required here
            // int mvLastRow = mvshet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row + 1; //+++++++++++++++++++++++++++++++++
            int rowcount = 2;
            int sr = 1;
            for (int index = 0; index < 15; index++)
            {
                //education_levelBO bo = list[index];//xxxxxxxxxxxxxxxxxxx
                exlwooksheet.Cells[rowcount, 1] = sr;
                exlwooksheet.Cells[rowcount, 2] = "1998";
                exlwooksheet.Cells[rowcount, 3] = "Male/Female";
                exlwooksheet.Cells[rowcount, 4] = agegroup[sr - 1];
                sr++;
                count = 5;
                for (var l = 0; l < (colNames.Count) - 4; l++)
                {
                    //erorr...............................
                    exlwooksheet.Cells[rowcount, count] = rand.Next(700, 7000);
                    count++;
                }
                rowcount++;
            }
            exceldoc.Save();
            exceldoc.Close();

            return Json("Done", JsonRequestBehavior.AllowGet);
        }

        public JsonResult getColumnNamemy(String y)
        {
            NDVEntities db = new NDVEntities();


            int id = (int)(from x in db.Attributes
                           where x.Attribute_Name.Equals(y)
                           select x.Attribute_ID).First();

            var colNames = (from x in db.Sub_Attribute
                            where x.Attribute_ID == id
                            select x.SubAttr_Name).ToList();

            colNames.Insert(0, "sr.");
            colNames.Insert(1, "Year.");
            colNames.Insert(2, "Gender.");
            colNames.Insert(3, "Age Group.");

            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }

        //---------------------------------------------------------------------------

        public ActionResult Index()
        {
            uid = (int)Session["UID"];
            NDVEntities db = new NDVEntities();
            var data = (from x1 in db.Problem_Post
                        where x1.User_ID == uid
                        select x1).ToList();

            var Alldata = (from x2 in db.Problem_Post
                           select x2).ToList();

            ViewBag.data = data;
            ViewBag.Alldata = Alldata;
            ViewBag.uname = Session["UN"];
            ViewBag.ud = Session["UID"];

            return View();
        }

        public ActionResult HomePage()
        {
            Session["MailID"] = "";
            Session["UN"] = "";
            Session["UID"] = "";
            return View();
        }

        public ActionResult ManualDataEntry()
        {
            return View();
        }

        public ActionResult AllProblemPosts()
        {
            uid = (int)Session["UID"];
            NDVEntities db = new NDVEntities();
            var data = (from x1 in db.Problem_Post
                        where x1.User_ID == uid
                        select x1).ToList();

            var Alldata = (from x2 in db.Problem_Post
                           select x2).ToList();

            ViewBag.data = data;
            ViewBag.Alldata = Alldata;
            ViewBag.uname = Session["UN"];
            ViewBag.ud = Session["UID"];
            return View();
        }

        public ActionResult MyProblemPosts()
        {
            uid = (int)Session["UID"];
            NDVEntities db = new NDVEntities();
            var data = (from x1 in db.Problem_Post
                        where x1.User_ID == uid
                        select x1).ToList();

            ViewBag.data = data;
            ViewBag.uname = Session["UN"];
            ViewBag.ud = Session["UID"];
            return View();
        }

        public ActionResult ErrorPage()
        {
            Session["UN"] = "";
            return View();
        }

        [HttpPost]
        public ActionResult saveProblemPost()
        {
            NDVEntities db = new NDVEntities();
            Problem_Post pp = new Problem_Post();
            uid = (int)Session["UID"];
            var data = (from x1 in db.User_Info
                        where x1.User_ID == uid
                        select x1.User_ID).ToArray();

            pp.User_ID = data[0];
            pp.Problem = Request["msg1"];
            pp.Like = 0;
            pp.Dislike = 0;
            pp.Report = 0;
            pp.Time = System.DateTime.Now;
            db.Problem_Post.Add(pp);
            db.SaveChanges();

            return RedirectToAction("Index");
        }    //Problem

        [HttpPost]
        public ActionResult SaveMessages()
        {
            NDVEntities db = new NDVEntities();
            UserMessage u = new UserMessage();

            u.Username = Request["un"];
            u.Email = Request["em"];
            u.PhoneNo = Request["ph"];
            u.Message = Request["msg"];

            db.UserMessages.Add(u);
            db.SaveChanges();


            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult UserLogin(Models.User_Info userInfo)
        {
            bool status = false;
            NDVEntities db = new NDVEntities();
            //Session["UN"] = "Hamza";
            var mail = userInfo.Email;
            var word = userInfo.Password;

            var x = from z in db.User_Info
                    where (z.Email.Equals(mail) && z.Password.Equals(word))
                    select z;

            foreach (var ui in x)
            {
                if (ui.Email.Equals(mail))
                {
                    ViewBag.EmailAdd = ui.Email;
                    Session["MailID"] = ui.Email;
                    Session["UN"] = ui.First_Name+" "+ui.Last_Name;
                    Session["UID"] = ui.User_ID;
                    uid = ui.User_ID;
                    status = true;
                }
                else
                {
                    Session["MailID"] = "";
                    Session["UN"] = "";
                    Session["UID"] = 0;
                    status = false;
                }
            }

            if (status)
            {
                return this.Json(status, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return this.Json(status, JsonRequestBehavior.AllowGet);
            }
        } 

        public int idFromEmail(String nm)
        {
            NDVEntities db = new NDVEntities();
            var uid = 0;
            var ui = from z in db.User_Info
                    where z.Email.Equals(nm)
                    select z;

            foreach(var c in ui)
            {
                uid = c.User_ID;
            }
            return uid;
        }

        [HttpPost]
        public ActionResult GetRegistered()
        {
            NDVEntities db = new NDVEntities();
            User_Info u = new User_Info();

            u.First_Name = Request["fname"];
            u.Last_Name = Request["lname"];
            u.Email = Request["email"];
            u.Phone_No = Request["phone"];
            u.Password = Request["pswd"];
            u.CNIC_No = Request["cnic"];
            u.Job = Request["job"];
            u.Location = Request["loc"];
            u.Qualification = Request.Form["qualification"].ToString();
            //u.Gender = Request.Form["gender"].ToString();
            u.DOB = Request.Form["date"].ToString() + "/" + Request.Form["month"].ToString() + "/" + Request.Form["year"].ToString();

            db.User_Info.Add(u);
            db.SaveChanges();

            var ii = idFromEmail(u.Email);

            Session["MailID"] = u.Email;
            Session["UN"] = u.First_Name + " " + u.Last_Name;
            Session["UID"] = ii;

            return RedirectToAction("Index");

        }

        public ActionResult EditProfile()
        {
            var ee = Request["email"];
            NDVEntities db = new NDVEntities();
            User_Info u = new User_Info();//db.User_Info.First(y => y.Email.Equals(ee));

            u.First_Name = Request["fname"];
            u.Last_Name = Request["lname"];
            u.Email = Request["email"];
            u.Phone_No = Request["phone"];
            u.Password = Request["pswd"];
            u.CNIC_No = Request["cnic"];
            u.Job = Request["job"];
            u.Location = Request["loc"];
            u.Gender = Request["gender"];

            db.SaveChanges();

            var ii = idFromEmail(u.Email);

            Session["MailID"] = u.Email;
            Session["UN"] = u.First_Name + " " + u.Last_Name;
            Session["UID"] = ii;

            return RedirectToAction("Index");

        }

        public JsonResult UserExist(String n)
        {
            Console.WriteLine(n);
            NDVEntities db = new NDVEntities();
            bool result = true;

            User_Info ui = db.User_Info.First(y => y.Email.Equals(n));
            if (ui.Email.Equals(n))
            {
                result = true;
            }
            else
            {
                result = false;
            }
            return this.Json(result , JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult locationToLocation(Province pid)
        {
            NDVEntities db = new NDVEntities();
            Array Result_List1;
            Array Result_List2;
            if(pid.Province_ID == 1)
            {
                Result_List1 = (from x in db.Provinces
                              select x.Province_Name).ToArray();

                Result_List2 = (from x in db.Provinces
                                select x.Province_ID).ToArray();
            }
            else if(pid.Province_ID == 2)
            {
                Result_List1 = (from x in db.Districts
                                select x.District_Name).ToArray();

                Result_List2 = (from x in db.Districts
                                select x.District_ID).ToArray();
            }
            else
            {
                Result_List1 = (from x in db.Tehsils
                                select x.Tehsil_Name).ToArray();

                Result_List2 = (from x in db.Tehsils
                                select x.Tehsil_ID).ToArray();
            }

            object obj = new
            {
                Result1 = Result_List1,
                Result2 = Result_List2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult provinceToDistrict(District pid)
        {
            //int id = int.Parse(pid);
            NDVEntities db = new NDVEntities();

            var disList1 = (from x in db.Districts
                            where x.Province_ID == pid.Province_ID
                           select x.District_Name).ToArray();

            var disList2 = (from x in db.Districts
                            where x.Province_ID == pid.Province_ID
                            select x.District_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult districtToTehsil(Tehsil did)
        {
            //int id = int.Parse(did);
            NDVEntities db = new NDVEntities();

            var disList1 = (from x in db.Tehsils
                           where x.District_ID == did.District_ID
                           select x.Tehsil_Name).ToArray();

            var disList2 = (from x in db.Tehsils
                           where x.District_ID == did.District_ID
                           select x.Tehsil_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult tehsilToTown(Town tid)
        {
            //int id = int.Parse(tid);
            NDVEntities db = new NDVEntities();

            var disList1 = (from x in db.Towns
                           where x.Tehsil_ID == tid.Tehsil_ID
                           select x.Town_Name).ToArray();

            var disList2 = (from x in db.Towns
                           where x.Tehsil_ID == tid.Tehsil_ID
                           select x.Town_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult attToSubAttr(MapSuite_using_MVC.Models.Attribute a)
        {
            //int id = int.Parse(tid);
            NDVEntities db = new NDVEntities();
            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == a.Attribute_Name));

            var disList1 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_Name).ToArray();

            var disList2 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult townToUC(Union_Council uid)
        {
            //int id = int.Parse(uid);
            //NDVEntities db = new NDVEntities();

            //var disList1 = (from x in db.Union_Council
            //               where x.Town_ID == uid.Town_ID
            //               select x.UC_Name).ToArray();

            //var disList2 = (from x in db.Union_Council
            //               where x.Town_ID == uid.Town_ID
            //               select x.UC_ID).ToArray();

            //object obj = new
            //{
            //    Result1 = disList1,
            //    Result2 = disList2
            //};

            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult changeAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            Location loc = db.Locations.First(z => (z.Province_ID == hlp.Prov_ID) && (z.District_ID == hlp.Dist_ID) && (z.Tehsil_ID == hlp.Teh_ID) && (z.UC_ID == null));
            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            Scenerio sc1 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));
            Scenerio sc2 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));
            Scenerio sc3 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));

            var disList11 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc1.Scenerio_ID
                            select x.M_Attr_Name).ToArray();

            var disList12 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc1.Scenerio_ID
                            select x.M_Attr_Value).ToArray();

            var disList21 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc2.Scenerio_ID
                             select x.M_Attr_Name).ToArray();

            var disList22 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc2.Scenerio_ID
                            select x.M_Attr_Value).ToArray();

            var disList31 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc3.Scenerio_ID
                             select x.M_Attr_Name).ToArray();

            var disList32 = (from x in db.Multi_Value
                            where x.Scenerio_ID == sc3.Scenerio_ID
                            select x.M_Attr_Value).ToArray();

            String[] Years;
            Years = new String[3];
            Years[0] = "2007";
            Years[1] = "2008";
            Years[2] = "2009";

            Object[] lab;
            lab = new Object[7];

            lab[0] = Years;
            lab[1] = disList11;
            lab[2] = disList12;
            lab[3] = disList21;
            lab[4] = disList22;
            lab[5] = disList31;
            lab[6] = disList32;

            return this.Json(lab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult changeSubAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            Location loc = db.Locations.First(z => (z.Province_ID == hlp.Prov_ID) && (z.District_ID == hlp.Dist_ID) && (z.Tehsil_ID == hlp.Teh_ID) && (z.UC_ID == null));
            Sub_Attribute sat = db.Sub_Attribute.First(z => (z.SubAttr_ID == hlp.SubAttr));

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name)) ;

            var sc1 = (from zz in db.Scenerios
                       where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == loc.Location_ID))
                     select zz.Scenerio_ID).ToList();

            var AgeRange = (from zz in db.Scenerios
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == loc.Location_ID))
                           select zz.Age).ToArray();

            long sum = 0;
            List<long> l;
            l = new List<long>();
            foreach (var xy in sc1)
            {
                Multi_Value m = db.Multi_Value.First(y => (y.Scenerio_ID == xy) && (y.M_Attr_Name.Equals(sat.SubAttr_Name)));
                //sum += (long) m.M_Attr_Value;
                l.Add((long) m.M_Attr_Value);
            }
            
            //Scenerio sc1 = (db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male"))));
            Scenerio sc2 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male")));
            //Scenerio sc3 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male")));

            var disList11 = (from x in db.Multi_Value
                             where (x.Scenerio_ID == sc2.Scenerio_ID)
                             select x.M_Attr_Name).ToArray();

            long[] disList12;
            disList12 = new long[4];
            disList12[0] = sum;
            disList12[1] = sum;
            disList12[2] = sum;
            disList12[3] = sum;

            //var disList12 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            //var disList21 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Name).ToArray();

            //var disList22 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            //var disList31 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc3.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Name).ToArray();

            //var disList32 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc3.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            String[] Years;
            Years = new String[3];
            Years[0] = "2007";
            Years[1] = "2008";
            Years[2] = "2009";

            Object[] lab;
            lab = new Object[7];

            lab[0] = Years;
            lab[1] = AgeRange;
            lab[2] = l.ToArray();
            lab[3] = AgeRange;
            lab[4] = l.ToArray();
            lab[5] = AgeRange;
            lab[6] = l.ToArray();

            return this.Json(lab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where (yy.Province_ID == hlp.Prov_ID)
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < ListOfMultiValues.Count; k = k + colName.Length)
                {
                    sum += ListOfMultiValues.ElementAt(k);
                }
                SumOfValues.Add(sum);   // alag alag values
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = colName;        // Names
            NamesAndValues[1] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceAndDistrictWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var element = location_List.ElementAt(i);

                var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                           select zz.Scenerio_ID).ToList();

                for (int j = 0; j < sc1.Count; j++)     // Multi Values
                {
                    var element1 = sc1.ElementAt(j);
                    var Records = (from x in db.Multi_Value
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records);
                }
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < l.Count; k = k + colName.Length)
                {
                    sum += l.ElementAt(k);
                }
                SumOfValues.Add(sum);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = colName;        // Names
            NamesAndValues[1] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProvinceDistrictTehsilWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var element = location_List.ElementAt(i);

                var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                           select zz.Scenerio_ID).ToList();

                for (int j = 0; j < sc1.Count; j++)     // Multi Values
                {
                    var element1 = sc1.ElementAt(j);
                    var Records = (from x in db.Multi_Value
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records);
                }
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < l.Count; k = k + colName.Length)
                {
                    sum += l.ElementAt(k);
                }
                SumOfValues.Add(sum);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = colName;        // Names
            NamesAndValues[1] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where (yy.Province_ID == hlp.Prov_ID)
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;

            List<Array> AllRecordsMale = new List<Array>();
            List<Array> AllRecordsFemale = new List<Array>();

            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                var Scenerio_List2 = (from zz in db.Scenerios        // 13
                                      where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Female")) && (zz.Location_ID == Location_Ids))
                                      select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var element2 = Scenerio_List2.ElementAt(j);

                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    var Records2 = (from x in db.Multi_Value     // 13
                                    where (x.Scenerio_ID == element2)
                                    select x.M_Attr_Value).ToArray();

                    AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
                    AllRecordsFemale.Add(Records2);
                }
            }

            //--------------------------------------------------------------------

            List<long> ListOfMultiValuesMale = new List<long>();
            List<long> ListOfMultiValuesFemale = new List<long>();

            for (int m = 0; m < AllRecordsMale.Count; m++)
            {
                Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
                Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
                for (int k = 0; k < Scenerio_RecordMale.Length; k++)
                {
                    ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
                    ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
                }
            }
            long sumOfMaleRecord = 0;
            long sumOfFemaleRecord = 0;
            // sub ki id
            for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
            {
                sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
                sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[3];

            NamesAndValues[0] = SubAttName;     // Names
            NamesAndValues[1] = sumOfMaleRecord;            // Values
            NamesAndValues[2] = sumOfFemaleRecord;

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceAndDistrictWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;

            List<Array> AllRecordsMale = new List<Array>();
            List<Array> AllRecordsFemale = new List<Array>();

            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                var Scenerio_List2 = (from zz in db.Scenerios        // 13
                                      where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Female")) && (zz.Location_ID == Location_Ids))
                                      select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var element2 = Scenerio_List2.ElementAt(j);

                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    var Records2 = (from x in db.Multi_Value     // 13
                                    where (x.Scenerio_ID == element2)
                                    select x.M_Attr_Value).ToArray();

                    AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
                    AllRecordsFemale.Add(Records2);
                }
            }

            //--------------------------------------------------------------------

            List<long> ListOfMultiValuesMale = new List<long>();
            List<long> ListOfMultiValuesFemale = new List<long>();

            for (int m = 0; m < AllRecordsMale.Count; m++)
            {
                Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
                Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
                for (int k = 0; k < Scenerio_RecordMale.Length; k++)
                {
                    ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
                    ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
                }
            }
            long sumOfMaleRecord = 0;
            long sumOfFemaleRecord = 0;
            // sub ki id
            for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
            {
                sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
                sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[3];

            NamesAndValues[0] = SubAttName;     // Names
            NamesAndValues[1] = sumOfMaleRecord;            // Values
            NamesAndValues[2] = sumOfFemaleRecord;

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProvinceDistrictTehsilWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
                                 select yy.Location_ID).ToList();

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;

            List<Array> AllRecordsMale = new List<Array>();
            List<Array> AllRecordsFemale = new List<Array>();

            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                var Scenerio_List2 = (from zz in db.Scenerios        // 13
                                      where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Female")) && (zz.Location_ID == Location_Ids))
                                      select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var element2 = Scenerio_List2.ElementAt(j);

                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    var Records2 = (from x in db.Multi_Value     // 13
                                    where (x.Scenerio_ID == element2)
                                    select x.M_Attr_Value).ToArray();

                    AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
                    AllRecordsFemale.Add(Records2);
                }
            }

            //--------------------------------------------------------------------

            List<long> ListOfMultiValuesMale = new List<long>();
            List<long> ListOfMultiValuesFemale = new List<long>();

            for (int m = 0; m < AllRecordsMale.Count; m++)
            {
                Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
                Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
                for (int k = 0; k < Scenerio_RecordMale.Length; k++)
                {
                    ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
                    ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
                }
            }
            long sumOfMaleRecord = 0;
            long sumOfFemaleRecord = 0;
            // sub ki id
            for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
            {
                sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
                sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[3];

            NamesAndValues[0] = SubAttName;                 // Names
            NamesAndValues[1] = sumOfMaleRecord;            // Values
            NamesAndValues[2] = sumOfFemaleRecord;

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult OnlyProvinceWithAttribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where (yy.Province_ID == hlp.Prov_ID)
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

        //    var colName = (from x in db.Sub_Attribute      // sub-attribute column names
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    List<Array> AllRecords = new List<Array>();
        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var Location_Ids = location_List.ElementAt(i); //20

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecords.Add(Records); // ( 13 * 4 ) * 20
        //        }
        //    }

        //    List<long> ListOfMultiValues = new List<long>();

        //    for (int m = 0; m < AllRecords.Count; m++)
        //    {
        //        Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
        //        for (int k = 0; k < Scenerio_Record.Length; k++)
        //        {
        //            ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
        //        }
        //    }

        //    List<long> SumOfValues = new List<long>();
        //    for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
        //    {
        //        long sum = 0;
        //        for (int k = n; k < ListOfMultiValues.Count; k = k + colName.Length)
        //        {
        //            sum += ListOfMultiValues.ElementAt(k);
        //        }
        //        SumOfValues.Add(sum);   // alag alag values
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[2];

        //    NamesAndValues[0] = colName;        // Names
        //    NamesAndValues[1] = SumOfValues;    // Values

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult OnlyProvinceAndDistrictWithAttribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

        //    var colName = (from x in db.Sub_Attribute      // sub-attribute column names
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    List<Array> AllRecords = new List<Array>();
        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var element = location_List.ElementAt(i);

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecords.Add(Records);
        //        }
        //    }

        //    List<long> l = new List<long>();

        //    for (int m = 0; m < AllRecords.Count; m++)
        //    {
        //        Array fir = AllRecords.ElementAt(m);
        //        for (int k = 0; k < fir.Length; k++)
        //        {
        //            l.Add((long)fir.GetValue(k));   // List of all values
        //        }
        //    }

        //    List<long> SumOfValues = new List<long>();
        //    for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
        //    {
        //        long sum = 0;
        //        for (int k = n; k < l.Count; k = k + colName.Length)
        //        {
        //            sum += l.ElementAt(k);
        //        }
        //        SumOfValues.Add(sum);
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[2];

        //    NamesAndValues[0] = colName;        // Names
        //    NamesAndValues[1] = SumOfValues;    // Values

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ProvinceDistrictTehsilWithAttribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

        //    var colName = (from x in db.Sub_Attribute      // sub-attribute column names
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    List<Array> AllRecords = new List<Array>();
        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var element = location_List.ElementAt(i);

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecords.Add(Records);
        //        }
        //    }

        //    List<long> l = new List<long>();

        //    for (int m = 0; m < AllRecords.Count; m++)
        //    {
        //        Array fir = AllRecords.ElementAt(m);
        //        for (int k = 0; k < fir.Length; k++)
        //        {
        //            l.Add((long)fir.GetValue(k));   // List of all values
        //        }
        //    }

        //    List<long> SumOfValues = new List<long>();
        //    for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
        //    {
        //        long sum = 0;
        //        for (int k = n; k < l.Count; k = k + colName.Length)
        //        {
        //            sum += l.ElementAt(k);
        //        }
        //        SumOfValues.Add(sum);
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[2];

        //    NamesAndValues[0] = colName;        // Names
        //    NamesAndValues[1] = SumOfValues;    // Values

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult OnlyProvinceWithSub_Attribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where (yy.Province_ID == hlp.Prov_ID)
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
        //    MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

        //    var colName = (from x in db.Sub_Attribute
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    var SubAttName = from x in db.Sub_Attribute
        //                     where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
        //                     select x.SubAttr_Name;

        //    List<Array> AllRecordsMale = new List<Array>();
        //    List<Array> AllRecordsFemale = new List<Array>();

        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var Location_Ids = location_List.ElementAt(i); //20

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        var Scenerio_List2 = (from zz in db.Scenerios        // 13
        //                              where ((zz.Gender.Equals("Female")) && (zz.Year == 1998) && (zz.Age == null))
        //                              select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var element2 = Scenerio_List2.ElementAt(j);

        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            var Records2 = (from x in db.Multi_Value     // 13
        //                            where ((x.Scenerio_ID == element2) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                            select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
        //            AllRecordsFemale.Add(Records2);
        //        }
        //    }

        //    //--------------------------------------------------------------------

        //    List<long> ListOfMultiValuesMale = new List<long>();
        //    List<long> ListOfMultiValuesFemale = new List<long>();

        //    for (int m = 0; m < AllRecordsMale.Count; m++)
        //    {
        //        Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
        //        Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
        //        for (int k = 0; k < Scenerio_RecordMale.Length; k++)
        //        {
        //            ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
        //            ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
        //        }
        //    }
        //    long sumOfMaleRecord = 0;
        //    long sumOfFemaleRecord = 0;
        //    // sub ki id
        //    for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
        //    {
        //        sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
        //        sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[3];

        //    NamesAndValues[0] = SubAttName;     // Names
        //    NamesAndValues[1] = sumOfMaleRecord;            // Values
        //    NamesAndValues[2] = sumOfFemaleRecord;

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult OnlyProvinceAndDistrictWithSub_Attribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
        //    MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

        //    var colName = (from x in db.Sub_Attribute
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    var SubAttName = from x in db.Sub_Attribute
        //                     where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
        //                     select x.SubAttr_Name;

        //    List<Array> AllRecordsMale = new List<Array>();
        //    List<Array> AllRecordsFemale = new List<Array>();

        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var Location_Ids = location_List.ElementAt(i); //20

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        var Scenerio_List2 = (from zz in db.Scenerios        // 13
        //                              where ((zz.Gender.Equals("Female")) && (zz.Year == 1998) && (zz.Age == null))
        //                              select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var element2 = Scenerio_List2.ElementAt(j);

        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            var Records2 = (from x in db.Multi_Value     // 13
        //                            where ((x.Scenerio_ID == element2) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                            select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
        //            AllRecordsFemale.Add(Records2);
        //        }
        //    }

        //    //--------------------------------------------------------------------

        //    List<long> ListOfMultiValuesMale = new List<long>();
        //    List<long> ListOfMultiValuesFemale = new List<long>();

        //    for (int m = 0; m < AllRecordsMale.Count; m++)
        //    {
        //        Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
        //        Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
        //        for (int k = 0; k < Scenerio_RecordMale.Length; k++)
        //        {
        //            ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
        //            ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
        //        }
        //    }
        //    long sumOfMaleRecord = 0;
        //    long sumOfFemaleRecord = 0;
        //    // sub ki id
        //    for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
        //    {
        //        sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
        //        sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[3];

        //    NamesAndValues[0] = SubAttName;     // Names
        //    NamesAndValues[1] = sumOfMaleRecord;            // Values
        //    NamesAndValues[2] = sumOfFemaleRecord;

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult ProvinceDistrictTehsilWithSub_Attribute(Models.helping hlp)
        //{
        //    NDVEntities db = new NDVEntities();

        //    var location_List = (from yy in db.Locations
        //                         where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
        //                         select yy.Location_ID).ToList();

        //    MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
        //    MapSuite_using_MVC.Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

        //    var colName = (from x in db.Sub_Attribute
        //                   where x.Attribute_ID == attr.Attribute_ID
        //                   select x.SubAttr_Name).ToArray();

        //    var SubAttName = from x in db.Sub_Attribute
        //                     where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
        //                     select x.SubAttr_Name;

        //    List<Array> AllRecordsMale = new List<Array>();
        //    List<Array> AllRecordsFemale = new List<Array>();

        //    for (int i = 0; i < location_List.Count; i++)
        //    {
        //        var Location_Ids = location_List.ElementAt(i); //20

        //        var Scenerio_List = (from zz in db.Scenerios        // 13
        //                             where ((zz.Gender.Equals("Male")) && (zz.Year == 1998) && (zz.Age == null))
        //                             select zz.Scenerio_ID).ToList();

        //        var Scenerio_List2 = (from zz in db.Scenerios        // 13
        //                              where ((zz.Gender.Equals("Female")) && (zz.Year == 1998) && (zz.Age == null))
        //                              select zz.Scenerio_ID).ToList();

        //        for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
        //        {
        //            var element1 = Scenerio_List.ElementAt(j);
        //            var element2 = Scenerio_List2.ElementAt(j);

        //            var Records = (from x in db.Multi_Value     // 13
        //                           where ((x.Scenerio_ID == element1) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                           select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            var Records2 = (from x in db.Multi_Value     // 13
        //                            where ((x.Scenerio_ID == element2) && (x.Attribute_ID == attr.Attribute_ID) && (x.Location_ID == Location_Ids))
        //                            select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

        //            AllRecordsMale.Add(Records); // ( 13 * 4 ) * 20
        //            AllRecordsFemale.Add(Records2);
        //        }
        //    }

        //    //--------------------------------------------------------------------

        //    List<long> ListOfMultiValuesMale = new List<long>();
        //    List<long> ListOfMultiValuesFemale = new List<long>();

        //    for (int m = 0; m < AllRecordsMale.Count; m++)
        //    {
        //        Array Scenerio_RecordMale = AllRecordsMale.ElementAt(m);  // Group of 4 values
        //        Array Scenerio_RecordFemale = AllRecordsFemale.ElementAt(m);
        //        for (int k = 0; k < Scenerio_RecordMale.Length; k++)
        //        {
        //            ListOfMultiValuesMale.Add((long)Scenerio_RecordMale.GetValue(k));   // List of all values -- sara kuch isi mai
        //            ListOfMultiValuesFemale.Add((long)Scenerio_RecordFemale.GetValue(k));
        //        }
        //    }
        //    long sumOfMaleRecord = 0;
        //    long sumOfFemaleRecord = 0;
        //    // sub ki id
        //    for (int k = (hlp.SubAttr - 1); k < ListOfMultiValuesMale.Count; k = k + colName.Length)
        //    {
        //        sumOfMaleRecord += ListOfMultiValuesMale.ElementAt(k);
        //        sumOfFemaleRecord += ListOfMultiValuesFemale.ElementAt(k);
        //    }

        //    Object[] NamesAndValues;
        //    NamesAndValues = new Object[3];

        //    NamesAndValues[0] = SubAttName;                 // Names
        //    NamesAndValues[1] = sumOfMaleRecord;            // Values
        //    NamesAndValues[2] = sumOfFemaleRecord;

        //    return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public JsonResult UserRelatedData(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();
            var st = hlp.Attr_Name.ToString();

            Location locat = db.Locations.First(x => (x.Tehsil_ID == 1));
            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name.Equals(st)));
            
            var colName = (from x in db.Sub_Attribute      // sub-attribute column values
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();

            var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                       where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == locat.Location_ID))
                       select zz.Scenerio_ID).ToList();

            for (int j = 0; j < sc1.Count; j++)     // Multi Values
            {
                var element1 = sc1.ElementAt(j);
                var Records = (from x in db.Multi_Value
                               where x.Scenerio_ID == element1
                               select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                AllRecords.Add(Records);
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }
            long sum = 0;
            List<long> l2 = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                for (int k = n; k < l.Count; k = k + 4)
                {
                    sum += l.ElementAt(k);
                }
                l2.Add(sum);
            }

            Object[] lab;
            lab = new Object[2];

            lab[0] = colName;
            lab[1] = l2;

            return this.Json(lab, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult deletePost(Problem_Post pp)
        {
            bool flag=false;
            NDVEntities db = new NDVEntities();

            var s = db.Problem_Post.Find(pp.Id);
            //db.Problem_Post.Remove(s);
            //db.SaveChanges();

            if(s.Id == pp.Id)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            return this.Json(flag, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult saveComment(Coment co)
        {
            NDVEntities db = new NDVEntities();
            Coment c = new Coment();
            c.Coment_Text = co.Coment_Text;
            c.User_ID = co.User_ID;
            c.Post_ID = co.Post_ID;
            c.Like = 0;
            c.Dislike = 0;
            c.Rating = 0;
            c.Date = System.DateTime.Now.Date;
            c.Time = System.DateTime.Now.TimeOfDay;

            db.Coments.Add(c);
            db.SaveChanges();

            return this.Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RuntimeGraphs()
        {
            return View();
        }

        [HttpPost]
        public JsonResult insertManualData(int l)
        {
            NDVEntities db = new NDVEntities();

            var colNames = (from x in db.Sub_Attribute
                            where x.Attribute_ID == 1
                            select x.SubAttr_Name).ToList();

            colNames.Insert(0, "sr.");
            colNames.Insert(1, "Year.");
            colNames.Insert(2, "Gender.");
            colNames.Insert(3, "Age Group.");
           
            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getColumnName(int y)
        {
            NDVEntities db = new NDVEntities();
            int xx = (int)y;

            var colNames = (from x in db.Sub_Attribute
                            where x.Attribute_ID == xx
                            select x.SubAttr_Name).ToList();

            colNames.Insert(0, "Sr#");
            colNames.Insert(1, "Year");
            colNames.Insert(2, "Gender");
            colNames.Insert(3, "Age Group");

            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PlaceData(List<String> y, List<String> z, String name, String loc)
        {
            NDVEntities db = new NDVEntities();
            bool Attr_Exist = false;
            List<string> colNames = y;
            string[] ColumnNames = colNames.ElementAt(0).Split(',');

            List<string> colval = z;
            String[] ColumnValues0 = colval.ElementAt(0).Split(',');

            List<int> ColumnValues = new List<int>();
            for (int v = 0; v < ColumnValues0.Length; v++ )
            {
                try { 
                    ColumnValues.Add(int.Parse(ColumnValues0[v])); 
                }
                catch {
                    ColumnValues.Add(0);
                }            }
            int ss = ColumnValues.Count;
            //int No_of_records = (int)z.Length / (int)y.Count;
            
            //--------------------------------------------------
            //-------------------Attribute----------------------
            Models.Attribute pre = new Models.Attribute();
            try{
                pre = db.Attributes.First(yy => yy.Attribute_Name.Equals(name));
            }
            catch {
                pre = null;
            }
            
            if(pre == null)
            {
                Attr_Exist = false;

                Models.Attribute a = new Models.Attribute();
                a.Attribute_Name = name;
                a.Flag = 1;

                //db.Attributes.Add(a);
                //db.SaveChanges();
            }

            int Att_ID = 8;
            try
            {
                pre = db.Attributes.First(yy => yy.Attribute_Name.Equals(name));
                Att_ID = (int)pre.Attribute_ID;
                Attr_Exist = true;
            }
            catch
            {
                pre = null;
                Att_ID = 8;
            }
            //Models.Attribute pre2 = db.Attributes.First(yy => yy.Attribute_Name.Equals(name));
            //int Att_ID = (int) pre2.Attribute_ID;

            //--------------------------------------------------
            //-------------------Sub Attribute------------------

            Sub_Attribute sub = new Sub_Attribute();

            if (!Attr_Exist)
            {
                for (int i = 1; i < ColumnNames.Length; i++)
                {
                    sub.Attribute_ID = Att_ID;
                    sub.SubAttr_Name = ColumnNames.ElementAt(i);

                    //db.Sub_Attribute.Add(sub);
                    //db.SaveChanges();
                }
            }

            //----------------------------------------------------
            //-------------------Scenerio-------------------------
            List<int> ScenerioIDList = new List<int>();
            for (int c = 0; c < ColumnValues.Count; c = c + ColumnNames.Length)
            {
                Scenerio sc = new Scenerio();
                sc.Location_ID = 1;              // User Provided Location
                sc.Age = "A";
                sc.Gender = "B";
                sc.Year = ColumnValues.ElementAt(c);
                sc.Attribute_ID = Att_ID;        // ID of New Attribute

                Scenerio s = new Scenerio();
                int Sce_ID = 0;
                try{
                    var ms = from sel in db.Scenerios
                             where (sel.Year == sc.Year) && (sel.Location_ID == sc.Location_ID) && (sel.Age == "A") && (sel.Gender == "B")
                             select sel;

                    foreach(var b in ms)
                    {
                        s = b;
                    }
                    Sce_ID = (int)s.Scenerio_ID;
                    ScenerioIDList.Add(Sce_ID);
                }
                catch{
                    s = null;
                }

                if (s == null){
                    //db.Scenerios.Add(sc);
                    //db.SaveChanges();

                    try
                    {
                        var ms = from sel in db.Scenerios
                                 where (sel.Year == sc.Year) && (sel.Location_ID == sc.Location_ID) && (sel.Age == "A") && (sel.Gender == "B")
                                 select sel;
                        foreach (var b in ms)
                        {
                            s = b;
                        }
                        Sce_ID = (int)s.Scenerio_ID;
                        ScenerioIDList.Add(Sce_ID);
                    }
                    catch{
                        s = null;
                    }
                }
            }

            //----------------------------------------------------
            //----------------- New_Place_Data -------------------

            New_Place_Data pd = new New_Place_Data();

            List<List<int>> ListOfMultiValues = new List<List<int>>();

            for (int m = 1, i=1; m <= ColumnValues.Count; m = m + ColumnNames.Length, i++)
            {
                List<int> Listing = new List<int>();
                for (int k = m; k < (ColumnNames.Length * i); k++)
                {
                    Listing.Add(ColumnValues.ElementAt(k));
                }
                ListOfMultiValues.Add(Listing);   // List of all values
            }
            int kk = 0;

            for (int count = 1; count <= ColumnValues.Count/ColumnNames.Length; count++)    //0,4,8
            {
                try
                {
                    int scenId = ScenerioIDList.ElementAt(kk++);
                    List<int> Listing0 = ListOfMultiValues.ElementAt(count-1);

                    for (int count1 = 0; count1 < Listing0.Count ; count1++)
                    {
                        pd.Scenerio_ID = scenId;
                        pd.Multi_Value_Name = ColumnNames[count1+1];
                        pd.Multi_Value_Stats = Listing0.ElementAt(count1);

                        //db.New_Place_Data.Add(pd);
                        //db.SaveChanges();
                    }
                }
                catch { }
            }

            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult PeopleData(List<String> y, List<String> z, String name, String loc)
        {
            NDVEntities db = new NDVEntities();
            bool Attr_Exist = false;
            List<string> colNames = y;
            string[] ColumnNames = colNames.ElementAt(0).Split(',');

            List<string> colval = z;
            String[] ColumnValues = colval.ElementAt(0).Split(',');

            int ss = ColumnValues.Length;

            //--------------------------------------------------
            //-------------------Attribute----------------------
            Models.Attribute pre = new Models.Attribute();
            try
            {
                pre = db.Attributes.First(yy => yy.Attribute_Name.Equals(name));
            }
            catch
            {
                pre = null;
            }

            if (pre == null)
            {
                Attr_Exist = false;

                Models.Attribute a = new Models.Attribute();
                a.Attribute_Name = name;
                a.Flag = 1;

                //db.Attributes.Add(a);
                //db.SaveChanges();
            }

            int Att_ID = 8;
            try
            {
                pre = db.Attributes.First(yy => yy.Attribute_Name.Equals(name));
                Att_ID = (int)pre.Attribute_ID;
                Attr_Exist = true;
            }
            catch
            {
                pre = null;
                Att_ID = 8;
            }

            //--------------------------------------------------
            //-------------------Sub Attribute------------------

            Sub_Attribute sub = new Sub_Attribute();

            if (!Attr_Exist)
            {
                for (int i = 1; i < ColumnNames.Length; i++)
                {
                    sub.Attribute_ID = Att_ID;
                    sub.SubAttr_Name = ColumnNames.ElementAt(i);

                    //db.Sub_Attribute.Add(sub);
                    //db.SaveChanges();
                }
            }

            //----------------------------------------------------
            //-------------------Scenerio-------------------------
            List<int> ScenerioIDList = new List<int>();
            for (int c = 0; c < ColumnValues.Length; c = c + ColumnNames.Length)
            {
                Scenerio sc = new Scenerio();
                sc.Location_ID = 1;              // User Provided Location
                sc.Age = ColumnValues.ElementAt(c+1).ToString();
                sc.Gender = ColumnValues.ElementAt(c+2).ToString();
                sc.Year = int.Parse(ColumnValues[c]);
                sc.Attribute_ID = Att_ID;        // ID of New Attribute or Existing 

                Scenerio s = new Scenerio();
                int Sce_ID = 0;
                try
                {
                    var ms = from sel in db.Scenerios
                             where (sel.Year == sc.Year) && (sel.Location_ID == sc.Location_ID) && (sel.Age == sc.Age) && (sel.Gender == sc.Gender)
                             select sel;

                    foreach (var b in ms)
                    {
                        s = b;
                    }
                    Sce_ID = (int)s.Scenerio_ID;
                    ScenerioIDList.Add(Sce_ID);
                }
                catch
                {
                    s = null;
                }

                if (s == null)
                {
                    //db.Scenerios.Add(sc);
                    //db.SaveChanges();

                    try
                    {
                        var ms = from sel in db.Scenerios
                                 where (sel.Year == sc.Year) && (sel.Location_ID == sc.Location_ID) && (sel.Age == sc.Age) && (sel.Gender == sc.Gender)
                                 select sel;
                        foreach (var b in ms)
                        {
                            s = b;
                        }
                        Sce_ID = (int)s.Scenerio_ID;
                        ScenerioIDList.Add(Sce_ID);
                    }
                    catch
                    {
                        s = null;
                    }
                }
            }

            //----------------------------------------------------
            //----------------- New_People_Data -------------------

            New_People_Data pd = new New_People_Data();

            List<List<String>> ListOfMultiValues = new List<List<String>>();

            for (int m = 3, i = 1; m <= ColumnValues.Length; m = m + ColumnNames.Length, i++)
            {
                List<String> Listing = new List<String>();
                for (int k = m; k < (ColumnNames.Length * i); k++)
                {
                    Listing.Add(ColumnValues[k]);
                }
                ListOfMultiValues.Add(Listing);   // List of all values
            }
            int kk = 0;

            for (int count = 3; count <= ColumnValues.Length / ColumnNames.Length; count++)    //0,4,8
            {
                try
                {
                    int scenId = ScenerioIDList.ElementAt(kk++);
                    List<String> Listing0 = ListOfMultiValues[count - 1];

                    for (int count1 = 0; count1 < Listing0.Count; count1++)
                    {
                        pd.Scenerio_ID = scenId;
                        pd.Multi_Value_Name = ColumnNames[count1 + 1];
                        pd.Multi_Value_Stats = int.Parse(Listing0[count1]);

                        //db.New_People_Data.Add(pd);
                        //db.SaveChanges();
                    }
                }
                catch { }
            }

            //----------------------------------------------------

            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult getColumnNameOnly(int y)
        {
            NDVEntities db = new NDVEntities();
            int xx = (int)y;
            var colNames = (from x in db.Sub_Attribute
                            where x.Attribute_ID == xx
                            select x.SubAttr_Name).ToList();

            return this.Json(colNames, JsonRequestBehavior.AllowGet);
        }       // 35th Method

        //[HttpPost]
        //public JsonResult plotComparisonGraph(District did)
        //{
        //    NDVEntities db = new NDVEntities();

        //    Array disList1;
        //    Array disList2;

        //    object obj = new
        //    {
        //        Result1 = disList1,
        //        Result2 = disList2
        //    };

        //    return this.Json(obj, JsonRequestBehavior.AllowGet);
        //}
    }
}