﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ThinkGeo.MapSuite.Core;
using ThinkGeo.MapSuite.MvcEdition;
using MapSuite_using_MVC.Models;
namespace MapSuite_using_MVC.Controllers
{
    public class Map1Controller : Controller
    {
        //
        // GET: /Map1/


        Map Map1 = null;
        Map Map2 = null;
        public ActionResult Index3()
        {
            ViewBag.colorCount = 3;
            Map1 = new Map("map",
         new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage), 510);
            new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage);
            WorldMapKitWmsWebOverlay worldMapKitOverlay = new WorldMapKitWmsWebOverlay("WorldMapKitOverlay");
            Map1.CustomOverlays.Add(worldMapKitOverlay);



            Map1.MapTools.LoadingImage.ImageUri = new Uri(Server.MapPath(@"~/ShapeFiles/loading_logofinal_by_zegerdon-d60eb1v.gif"));
            Map1.MapTools.LoadingImage.Enabled = true;
            Map1.MapTools.LoadingImage.Height = 64;
            Map1.MapTools.LoadingImage.Width = 64;
            Map1.MapTools.ScaleLine.Enabled = true;
            Map1.MapUnit = GeographyUnit.DecimalDegree;
            Map1.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LongitudeLatitude;
            Map1.MapTools.MouseCoordinate.Enabled = true;
            Map1.Cursor = CursorType.Pointer;
            //Map1.MapBackground.BackgroundBrush = new GeoSolidBrush(GeoColor.GeographicColors.Swamp);


            ShapeFileFeatureLayer PakLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));
            PakLayer.ZoomLevelSet.ZoomLevel03.DefaultAreaStyle = AreaStyles.Country1;
            PakLayer.ZoomLevelSet.ZoomLevel03.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Crimson, 3, 3);//TextStyles.Capital3("NAME_1");
            PakLayer.ZoomLevelSet.ZoomLevel03.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level04;
            ShapeFileFeatureLayer.BuildIndexFile(Server.MapPath(@"~/ShapeFiles/PAK_adm0.shp"));


            ShapeFileFeatureLayer capitalLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"));
            capitalLayer.ZoomLevelSet.ZoomLevel05.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PaleGreen, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightOrange));
            capitalLayer.ZoomLevelSet.ZoomLevel05.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_1", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Green, 3, 3);//TextStyles.Capital3("NAME_1");
            capitalLayer.ZoomLevelSet.ZoomLevel05.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level06;

            ShapeFileFeatureLayer DivisionLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm2.shp"));
            DivisionLayer.ZoomLevelSet.ZoomLevel07.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("Name_2", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Chocolate, 3, 3);
            DivisionLayer.ZoomLevelSet.ZoomLevel07.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level08;
            DivisionLayer.ZoomLevelSet.ZoomLevel07.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PaleOrange, GeoColor.FromArgb(100, GeoColor.SimpleColors.BrightGreen));


            ShapeFileFeatureLayer districtLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
            //districtLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = TextStyles.Capital3("NAME_3");
            districtLayer.ZoomLevelSet.ZoomLevel08.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_3", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Black, 3, 3);
            districtLayer.ZoomLevelSet.ZoomLevel08.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level09;
            districtLayer.ZoomLevelSet.ZoomLevel08.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.PastelYellow, GeoColor.FromArgb(100, GeoColor.SimpleColors.DarkGreen));

            districtLayer.DrawingMarginPercentage = 50;
            ShapeFileFeatureLayer tahseelLayer = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles/PAK_Tehsil_Boundary.shp"));
            //tahseelLayer.ZoomLevelSet.ZoomLevel01.DefaultTextStyle = TextStyles.Capital3("NAME_3");
            tahseelLayer.ZoomLevelSet.ZoomLevel10.CustomStyles.Add(TextStyles.CreateSimpleTextStyle("TEHSIL", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Black, 3, 3));
            tahseelLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            tahseelLayer.ZoomLevelSet.ZoomLevel10.CustomStyles.Add(AreaStyles.CreateSimpleAreaStyle(GeoColor.SimpleColors.Transparent, GeoColor.FromArgb(100, GeoColor.SimpleColors.Black)));
            ShapeFileFeatureLayer.BuildIndexFile(Server.MapPath(@"~/ShapeFiles/PAK_Tehsil_Boundary.shp"));
            LayerOverlay staticOverlay = new LayerOverlay("StaticOverlay");
            staticOverlay.IsBaseOverlay = false;
            staticOverlay.Layers.Add("PakLayer", PakLayer);
            staticOverlay.Layers.Add("CapitalLayer", capitalLayer);
            staticOverlay.Layers.Add("DivisionLayer", DivisionLayer);
            staticOverlay.Layers.Add("DistrictLayer", districtLayer);
            staticOverlay.Layers.Add("TahseelLayer", tahseelLayer);


            districtLayer.Open();
            Map1.CurrentExtent = districtLayer.GetBoundingBox();
            Map1.CustomOverlays.Add(staticOverlay);
            districtLayer.Close();


            InMemoryFeatureLayer highlightLayer = new InMemoryFeatureLayer();
            highlightLayer.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(165, 42, 42, 100), GeoColor.GeographicColors.DeepOcean);
            highlightLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            InMemoryFeatureLayer highlightLayer2 = new InMemoryFeatureLayer();
            highlightLayer2.ZoomLevelSet.ZoomLevel07.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(140, 50, 30, 120), GeoColor.GeographicColors.Dirt);
            highlightLayer2.ZoomLevelSet.ZoomLevel07.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level08;
            InMemoryFeatureLayer highlightLayer3 = new InMemoryFeatureLayer();
            highlightLayer3.ZoomLevelSet.ZoomLevel08.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(160, 86, 120, 160), GeoColor.GeographicColors.Dirt);
            highlightLayer3.ZoomLevelSet.ZoomLevel08.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level09;
            LayerOverlay dynamicOverlay = new LayerOverlay("HightLightDynamicOverlay");
            dynamicOverlay.Layers.Add("HighLightLayer", highlightLayer);
            dynamicOverlay.Layers.Add("HighLightLayer2", highlightLayer2);
            dynamicOverlay.Layers.Add("HighLightLayer3", highlightLayer3);
            dynamicOverlay.IsBaseOverlay = false;
            Map1.CustomOverlays.Add(dynamicOverlay);
            //Map1.Popups.Add(new CloudPopup("information") { AutoSize = true, IsVisible = false });
            Map1.Popups.Add(new CloudPopup("information", Map1.CurrentExtent.GetCenterPoint()) { AutoSize = true, IsVisible = false });
            ViewBag.map = Map1;
            Session["map"] = Map1;
            /////------------------------------------------------------Map 2-------------------------------------------------------------
            Map2 = new Map("map2",
      new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage), 510);
            //new System.Web.UI.WebControls.Unit(100, System.Web.UI.WebControls.UnitType.Percentage);
            //WorldMapKitWmsWebOverlay worldMapKitOverlay = new WorldMapKitWmsWebOverlay("WorldMapKitOverlay");
            //Map2.CustomOverlays.Add(worldMapKitOverlay);



            Map2.MapTools.LoadingImage.ImageUri = new Uri(Server.MapPath(@"~/ShapeFiles2/loading_logofinal_by_zegerdon-d60eb1v.gif"));
            Map2.MapTools.LoadingImage.Enabled = true;
            Map2.MapTools.LoadingImage.Height = 64;
            Map2.MapTools.LoadingImage.Width = 64;
            Map2.MapTools.ScaleLine.Enabled = true;
            Map2.MapUnit = GeographyUnit.DecimalDegree;
            Map2.MapTools.MouseCoordinate.MouseCoordinateType = MouseCoordinateType.LongitudeLatitude;
            Map2.MapTools.MouseCoordinate.Enabled = true;
            Map2.Cursor = CursorType.Default;


            Map2.MapBackground.BackgroundBrush = new GeoSolidBrush(GeoColor.StandardColors.SlateGray);


            ShapeFileFeatureLayer PakLayer2 = new ShapeFileFeatureLayer(Server.MapPath(@"~/ShapeFiles2/PAK_adm0.shp"));
            PakLayer2.ZoomLevelSet.ZoomLevel06.DefaultAreaStyle = AreaStyles.Country1;
            PakLayer2.ZoomLevelSet.ZoomLevel06.DefaultTextStyle = TextStyles.CreateSimpleTextStyle("NAME_ENGLI", "Arial", 8, DrawingFontStyles.Italic, GeoColor.StandardColors.Crimson, 3, 3);//TextStyles.Capital3("NAME_1");
            PakLayer2.ZoomLevelSet.ZoomLevel06.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            ShapeFileFeatureLayer.BuildIndexFile(Server.MapPath(@"~/ShapeFiles2/PAK_adm0.shp"));



            LayerOverlay staticOverlay2 = new LayerOverlay("StaticOverlay2");
            staticOverlay2.IsBaseOverlay = false;
            staticOverlay2.Layers.Add("PakLayer2", PakLayer2);


            PakLayer2.Open();
            Map2.CurrentExtent = PakLayer2.GetBoundingBox();
            Map2.CustomOverlays.Add(staticOverlay2);

            PakLayer2.Close();

            //InMemoryFeatureLayer highlightLayer = new InMemoryFeatureLayer();
            //highlightLayer.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(165, 42, 42, 100), GeoColor.GeographicColors.DeepOcean);
            //highlightLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;

            //LayerOverlay dynamicOverlay = new LayerOverlay("HightLightDynamicOverlay");
            //dynamicOverlay.Layers.Add("HighLightLayer", highlightLayer);

            //dynamicOverlay.IsBaseOverlay = false;
            //Map2.CustomOverlays.Add(dynamicOverlay);

            Map2.Popups.Add(new CloudPopup("information", Map2.CurrentExtent.GetCenterPoint()) { AutoSize = true, IsVisible = false });
            ViewBag.map2 = Map2;
            return View();
        }


       
        [MapActionFilter]
        public string FindFeatureClick(Map map, GeoCollection<object> args)
        {
            PointShape position = new PointShape(double.Parse(args[0].ToString()), double.Parse(args[1].ToString()));

            LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            LayerOverlay dynamicOverlay = (LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"];

            ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers["TahseelLayer"]);
            ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["CapitalLayer"]);
            ShapeFileFeatureLayer shapeFileLayer3 = (ShapeFileFeatureLayer)(staticOverlay.Layers["DistrictLayer"]);
            //highLightLayer for tahsil
            InMemoryFeatureLayer highLightLayer = (InMemoryFeatureLayer)(dynamicOverlay.Layers["HighLightLayer"]);
            highLightLayer.ZoomLevelSet.ZoomLevel10.DefaultAreaStyle = AreaStyles.CreateSimpleAreaStyle(GeoColor.FromArgb(255, 243, 239, 228), GeoColor.StandardColors.CadetBlue, 1);
            highLightLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level12;
            highLightLayer.InternalFeatures.Clear();
            //highLightLayer for division
            InMemoryFeatureLayer highLightLayer2 = (InMemoryFeatureLayer)(dynamicOverlay.Layers["HighLightLayer2"]);
            highLightLayer2.InternalFeatures.Clear();
            //highLightLayer for district
            InMemoryFeatureLayer highLightLayer3 = (InMemoryFeatureLayer)(dynamicOverlay.Layers["HighLightLayer3"]);
            highLightLayer3.InternalFeatures.Clear();
            shapeFileLayer.Open();
            Collection<Feature> selectedFeatures = shapeFileLayer.QueryTools.GetFeaturesContaining(position, new string[] { "PROVINCE", "DISTRICT", "TEHSIL" });
            shapeFileLayer.Close();
            shapeFileLayer2.Open();
            Collection<Feature> selectedFeatures2 = shapeFileLayer2.QueryTools.GetFeaturesContaining(position, new string[] { "NAME_1", "TYPE_1" });
            shapeFileLayer2.Close();
            shapeFileLayer3.Open();
            Collection<Feature> selectedFeatures3 = shapeFileLayer3.QueryTools.GetFeaturesContaining(position, new string[] { "NAME_3" });
            shapeFileLayer3.Close();
            foreach (Feature feature in selectedFeatures)
            {
                highLightLayer.InternalFeatures.Add(feature.Id, feature);
            }
            foreach (Feature feature in selectedFeatures2)
            {
                highLightLayer2.InternalFeatures.Add(feature.Id, feature);
            }
            foreach (Feature feature in selectedFeatures3)
            {
                highLightLayer3.InternalFeatures.Add(feature.Id, feature);
            }
            return GetPopupContent(selectedFeatures);
        }

        private static string GetPopupContent(Collection<Feature> features)
        {
            string content;
            if (features.Count > 0)
            {
                StringBuilder message = new StringBuilder();
                message.AppendFormat("<li>PROVINCE NAME : {0}</li>", features[0].ColumnValues["PROVINCE"].Trim());
                message.AppendFormat("<li>DISTRICT NAME : {0}</li>", features[0].ColumnValues["DISTRICT"].Trim());
                message.AppendFormat("<li>TEHSIL NAME : {0}</li>", features[0].ColumnValues["TEHSIL"].Trim());

                string messageInPopup = String.Format("<div class='normalBlueTx'>{0}</div>", message.ToString());

                content = messageInPopup;
            }
            else
            {
                content = @"<div class='normalBlueTx'>Please click on a Tehsil to show its information.</div>";
            }
            return content;
        }
        [MapActionFilter]
        public JsonResult provinceToDivision(Map map, GeoCollection<object> args)
        {
            List<Object> divisionArray = new List<object>();

            string sql = null;
            DataTable dataTable = null;

            sql = "select ID_2, NAME_2 from PAK_adm2 where Name_1 ='" + args[1].ToString() + "'";

            ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm2.shp"));
            featureSource.Open();

            dataTable = featureSource.ExecuteQuery(sql);
            featureSource.Close();

            foreach (DataRow row in dataTable.Rows)
            {

                Object obj = new
                {
                    Division_ID = row["ID_2"].ToString(),
                    Division_Name = row["NAME_2"].ToString()
                };

                divisionArray.Add(obj);
            }



            //LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            //LayerOverlay dynamicOverlay = (LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"];
            //InMemoryFeatureLayer highLightLayer2 = (InMemoryFeatureLayer)(dynamicOverlay.Layers["HighLightLayer2"]);

            //FeatureLayer shapeFileLayer = (FeatureLayer)(staticOverlay.Layers["DivisionLayer"]);
            //shapeFileLayer.Open();
            //Feature selectedFeatures = shapeFileLayer.QueryTools.GetFeatureById(args[0].ToString(), new string[] { "NAME_2" });
            //shapeFileLayer.Close();
            //highLightLayer2.InternalFeatures.Clear();
            //highLightLayer2.InternalFeatures.Add(selectedFeatures.Id,selectedFeatures);


            string id = args[0] as string;
            FeatureLayer shapeFileLayer = (FeatureLayer)((LayerOverlay)map.CustomOverlays["StaticOverlay"]).Layers["DivisionLayer"];
            InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer2"];
            mapShapeLayer.IsVisible = true;
            shapeFileLayer.Open();
            Collection<Feature> features = shapeFileLayer.QueryTools.GetFeaturesByColumnValue("ID_1", id, ReturningColumnsType.AllColumns);
            //Feature feature = shapeFileLayer.FeatureSource.GetFeatureById(id, new string[] { "NAME_1", "TYPE_1" });
            shapeFileLayer.Close();

            mapShapeLayer.InternalFeatures.Clear();


            foreach (Feature feature in features)
            {
                mapShapeLayer.InternalFeatures.Add(feature.Id, feature);
            }
            return this.Json(divisionArray.ToArray(), JsonRequestBehavior.AllowGet);

        }

        [MapActionFilter]
        public JsonResult DivisionToDistrict(Map map, GeoCollection<object> args)
        {
            List<Object> districtArray = new List<object>();

            string sql = null;
            DataTable dataTable = null;

            sql = "select ID_3, NAME_3 from PAK_adm3 where NAME_2 ='" + args[1].ToString() + "'";

            ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
            featureSource.Open();

            dataTable = featureSource.ExecuteQuery(sql);
            featureSource.Close();

            foreach (DataRow row in dataTable.Rows)
            {

                Object obj = new
                {
                    District_ID = row["ID_3"].ToString(),
                    District_Name = row["NAME_3"].ToString()
                };

                districtArray.Add(obj);
            }



            //LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            //LayerOverlay dynamicOverlay = (LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"];
            //InMemoryFeatureLayer highLightLayer2 = (InMemoryFeatureLayer)(dynamicOverlay.Layers["HighLightLayer2"]);

            //FeatureLayer shapeFileLayer = (FeatureLayer)(staticOverlay.Layers["DivisionLayer"]);
            //shapeFileLayer.Open();
            //Feature selectedFeatures = shapeFileLayer.QueryTools.GetFeatureById(args[0].ToString(), new string[] { "NAME_2" });
            //shapeFileLayer.Close();
            //highLightLayer2.InternalFeatures.Clear();
            //highLightLayer2.InternalFeatures.Add(selectedFeatures.Id,selectedFeatures);


            //string id = args[0] as string;
            //FeatureLayer shapeFileLayer = (FeatureLayer)((LayerOverlay)map.CustomOverlays["StaticOverlay"]).Layers["DivisionLayer"];
            //InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer2"];

            //shapeFileLayer.Open();
            //Feature feature = shapeFileLayer.FeatureSource.GetFeatureById(id, new string[] { "NAME_1", "TYPE_1" });
            //shapeFileLayer.Close();

            //mapShapeLayer.InternalFeatures.Clear();
            //mapShapeLayer.InternalFeatures.Add(feature.Id, feature);
            string id = args[0] as string;
            FeatureLayer shapeFileLayer = (FeatureLayer)((LayerOverlay)map.CustomOverlays["StaticOverlay"]).Layers["DistrictLayer"];
            InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer3"];

            shapeFileLayer.Open();
            Collection<Feature> features = shapeFileLayer.QueryTools.GetFeaturesByColumnValue("ID_2", id, ReturningColumnsType.AllColumns);
            //Feature feature = shapeFileLayer.FeatureSource.GetFeatureById(id, new string[] { "NAME_1", "TYPE_1" });
            shapeFileLayer.Close();

            mapShapeLayer.InternalFeatures.Clear();


            foreach (Feature feature in features)
            {
                mapShapeLayer.InternalFeatures.Add(feature.Id, feature);
            }

            return this.Json(districtArray.ToArray(), JsonRequestBehavior.AllowGet);

        }
        [MapActionFilter]
        public JsonResult DistrictToTahsil(Map map, GeoCollection<object> args)
        {
            List<Object> tehsilArray = new List<object>();

            string sql = null;
            DataTable dataTable = null;

            sql = "select TEHSIL from Pak_Tehsil_Boundary where DISTRICT ='" + args[1].ToString() + "'";

            ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/Pak_Tehsil_Boundary.shp"));
            featureSource.Open();

            dataTable = featureSource.ExecuteQuery(sql);
            featureSource.Close();

            foreach (DataRow row in dataTable.Rows)
            {

                Object obj = new
                {
                    Tehsil_Name = row["TEHSIL"].ToString()
                };

                tehsilArray.Add(obj);
            }






            return this.Json(tehsilArray.ToArray(), JsonRequestBehavior.AllowGet);

        }
        [MapActionFilter]
        public string ZoomToShape(Map map, GeoCollection<object> args)
        {
            PointShape position = new PointShape(double.Parse(args[0].ToString()), double.Parse(args[1].ToString()));
            LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["CapitalLayer"]);

            shapeFileLayer2.Open();
            Collection<Feature> features = shapeFileLayer2.QueryTools.GetFeaturesContaining(position, ReturningColumnsType.AllColumns);
            shapeFileLayer2.Close();


            RectangleShape extent = map.CurrentExtent;
            if (features.Count > 0)
            {

                map.CurrentExtent = ExtentHelper.GetDrawingExtent(features[0].GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                extent = features[0].GetBoundingBox();


            }

            string extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            return extentString + "|" + "0";
        }

        [MapActionFilter]
        public string FindFeature(Map map, GeoCollection<object> args)
        {
            string extentString = null;
            string id = args[0] as string;
            string sf = args[1] as string;
            if (sf == "NAME_1")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["CapitalLayer"]);
                shapeFileLayer2.Open();
                Collection<Feature> features = shapeFileLayer2.QueryTools.GetFeaturesByColumnValue("ID_1", id, ReturningColumnsType.AllColumns);

                shapeFileLayer2.Close();

                RectangleShape extent = map.CurrentExtent;
                if (features.Count > 0)
                {

                    map.CurrentExtent = ExtentHelper.GetDrawingExtent(features[0].GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                    extent = features[0].GetBoundingBox();


                }



                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }
            if (sf == "NAME_2")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["DivisionLayer"]);
                shapeFileLayer2.Open();
                Collection<Feature> features = shapeFileLayer2.QueryTools.GetFeaturesByColumnValue("ID_2", id, ReturningColumnsType.AllColumns);
                shapeFileLayer2.Close();

                RectangleShape extent = map.CurrentExtent;
                if (features.Count > 0)
                {

                    map.CurrentExtent = ExtentHelper.GetDrawingExtent(features[0].GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                    extent = features[0].GetBoundingBox();


                }
                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }
            if (sf == "NAME_3")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["DistrictLayer"]);
                shapeFileLayer2.Open();
                Collection<Feature> features = shapeFileLayer2.QueryTools.GetFeaturesByColumnValue("ID_3", id, ReturningColumnsType.AllColumns);
                shapeFileLayer2.Close();

                RectangleShape extent = map.CurrentExtent;
                if (features.Count > 0)
                {

                    map.CurrentExtent = ExtentHelper.GetDrawingExtent(features[0].GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                    extent = features[0].GetBoundingBox();


                }
                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }
            if (sf == "TEHSIL")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["TahseelLayer"]);
                shapeFileLayer2.Open();
                Collection<Feature> features = shapeFileLayer2.QueryTools.GetFeaturesByColumnValue("TEHSIL", id, ReturningColumnsType.AllColumns);
                shapeFileLayer2.Close();
                InMemoryFeatureLayer highLightLayer3 = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer3"];
                highLightLayer3.InternalFeatures.Clear();
                RectangleShape extent = map.CurrentExtent;
                if (features.Count > 0)
                {

                    map.CurrentExtent = ExtentHelper.GetDrawingExtent(features[0].GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                    extent = features[0].GetBoundingBox();


                }
                foreach (Feature feature in features)
                {
                    highLightLayer3.InternalFeatures.Add(feature.Id, feature);
                }

                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }

            return extentString + "|" + "0";
        }
        [MapActionFilter]
        public string FindFeature2(Map map, GeoCollection<object> args)
        {
            string extentString = null;
            
            string sf = args[0] as string;
            if (sf == "NAME_1")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["CapitalLayer"]);
               
                RectangleShape extent = map.CurrentExtent;
               

                    map.CurrentExtent = ExtentHelper.GetDrawingExtent(shapeFileLayer2.GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                    extent = shapeFileLayer2.GetBoundingBox();



                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }
            if (sf == "NAME_2")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["DivisionLayer"]);
                RectangleShape extent = map.CurrentExtent;


                map.CurrentExtent = ExtentHelper.GetDrawingExtent(shapeFileLayer2.GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                extent = shapeFileLayer2.GetBoundingBox();



                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);

            }
            if (sf == "NAME_3")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["DistrictLayer"]);
                RectangleShape extent = map.CurrentExtent;


                map.CurrentExtent = ExtentHelper.GetDrawingExtent(shapeFileLayer2.GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                extent = shapeFileLayer2.GetBoundingBox();



                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);
            }
            if (sf == "TEHSIL")
            {
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay2"];
                ShapeFileFeatureLayer shapeFileLayer2 = (ShapeFileFeatureLayer)(staticOverlay.Layers["TahseelLayer"]);
                RectangleShape extent = map.CurrentExtent;


                map.CurrentExtent = ExtentHelper.GetDrawingExtent(shapeFileLayer2.GetBoundingBox(), (float)map.WidthInPixels, (float)map.HeightInPixels);

                extent = shapeFileLayer2.GetBoundingBox();



                extentString = string.Format(System.Globalization.CultureInfo.InvariantCulture, "{0},{1},{2},{3}", extent.LowerLeftPoint.X, extent.LowerLeftPoint.Y, extent.UpperRightPoint.X, extent.UpperRightPoint.Y);
            }

            return extentString + "|" + "0";
        }
        [HttpPost]
        public JsonResult attToSubAttr(MapSuite_using_MVC.Models.Attribute a)
        {
            //int id = int.Parse(tid);
            NDVEntities db = new NDVEntities();
            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == a.Attribute_Name));

            var disList1 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_Name).ToArray();

            var disList2 = (from x in db.Sub_Attribute
                            where x.Attribute_ID == attr.Attribute_ID
                            select x.SubAttr_ID).ToArray();

            object obj = new
            {
                Result1 = disList1,
                Result2 = disList2
            };

            return this.Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]//get data attribute change only work for prov dist and teh is selected
        public JsonResult changeAttribute(Models.helping hlp)
        { 
            //only work for prov dist and teh
            NDVEntities db = new NDVEntities();
            var prov = db.DbfToNDVs.First(p => (p.Dbf_ID == hlp.Prov_ID));
            var dis = db.DbfToNDVs.First(d => (d.Dbf_ID == hlp.Dist_ID));
            //var teh = db.DbfToNDVs.First(t=>(t.Dbf_Loc_Name==hlp.Teh_Name));
            Location loc = db.Locations.First(z => (z.Province_ID == prov.NDV_ID) && (z.District_ID == dis.NDV_ID) && (z.Tehsil_ID == 1) && (z.UC_ID == null));
            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            Scenerio sc1 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));
            Scenerio sc2 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));
            Scenerio sc3 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID));

            var disList11 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc1.Scenerio_ID
                             select x.M_Attr_Name).ToArray();

            var disList12 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc1.Scenerio_ID
                             select x.M_Attr_Value).ToArray();

            var disList21 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc2.Scenerio_ID
                             select x.M_Attr_Name).ToArray();

            var disList22 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc2.Scenerio_ID
                             select x.M_Attr_Value).ToArray();

            var disList31 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc3.Scenerio_ID
                             select x.M_Attr_Name).ToArray();

            var disList32 = (from x in db.Multi_Value
                             where x.Scenerio_ID == sc3.Scenerio_ID
                             select x.M_Attr_Value).ToArray();

            String[] Years;
            Years = new String[3];
            Years[0] = "2007";
            Years[1] = "2008";
            Years[2] = "2009";

            Object[] lab;
            lab = new Object[7];

            lab[0] = Years;
            lab[1] = disList11;
            lab[2] = disList12;
            lab[3] = disList21;
            lab[4] = disList22;
            lab[5] = disList31;
            lab[6] = disList32;

            return this.Json(lab, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]//show data when prov dist and teh selected with attr and sub attr
        public JsonResult changeSubAttribute2(MapSuite_using_MVC.Models.helping hlp)
        {

            NDVEntities db = new NDVEntities();
            var prov = db.DbfToNDVs.First(p => (p.Dbf_ID == hlp.Prov_ID));
            var dis = db.DbfToNDVs.First(d => (d.Dbf_ID == hlp.Dist_ID));
            //var teh = db.DbfToNDVs.First(t=>(t.Dbf_Loc_Name==hlp.Teh_Name));
            Location loc = db.Locations.First(z => (z.Province_ID == prov.NDV_ID) && (z.District_ID == dis.NDV_ID) && (z.Tehsil_ID == 1) && (z.UC_ID == null));
            Sub_Attribute sat = db.Sub_Attribute.First(z => (z.SubAttr_ID == hlp.SubAttr));

            MapSuite_using_MVC.Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var sc1 = (from zz in db.Scenerios
                       where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == loc.Location_ID))
                       select zz.Scenerio_ID).ToList();

            var AgeRange = (from zz in db.Scenerios
                            where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == loc.Location_ID))
                            select zz.Age).ToArray();

            long sum = 0;
            List<long> l;
            l = new List<long>();
            foreach (var xy in sc1)
            {
                Multi_Value m = db.Multi_Value.First(y => (y.Scenerio_ID == xy) && (y.M_Attr_Name.Equals(sat.SubAttr_Name)));
                //sum += (long) m.M_Attr_Value;
                l.Add((long)m.M_Attr_Value);
            }

            //Scenerio sc1 = (db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male"))));
            Scenerio sc2 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male")));
            //Scenerio sc3 = db.Scenerios.First(y => (y.Attribute_ID == attr.Attribute_ID) && (y.Location_ID == loc.Location_ID) && (y.Gender.Equals("Male")));

            var disList11 = (from x in db.Multi_Value
                             where (x.Scenerio_ID == sc2.Scenerio_ID)
                             select x.M_Attr_Name).ToArray();

            long[] disList12;
            disList12 = new long[4];
            disList12[0] = sum;
            disList12[1] = sum;
            disList12[2] = sum;
            disList12[3] = sum;

            //var disList12 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            //var disList21 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Name).ToArray();

            //var disList22 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc2.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            //var disList31 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc3.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Name).ToArray();

            //var disList32 = (from x in db.Multi_Value
            //                 where (x.Scenerio_ID == sc3.Scenerio_ID && x.M_Attr_Name.Equals(hlp.SubAttr))
            //                 select x.M_Attr_Value).ToArray();

            String[] disList0;
            disList0 = new String[3];
            disList0[0] = "2007";
            disList0[1] = "2008";
            disList0[2] = "2009";

            Object[] lab;
            lab = new Object[7];

            lab[0] = disList0;
            lab[1] = AgeRange;
            lab[2] = l.ToArray();
            lab[3] = AgeRange;
            lab[4] = l.ToArray();
            lab[5] = AgeRange;
            lab[6] = l.ToArray();

            return this.Json(lab, JsonRequestBehavior.AllowGet);
        }

       
        [MapActionFilter]//position of location ie( lat lon)
        public JsonResult changeSubAttribute(Map map, GeoCollection<object> args)
        {
            // prov    05------>06
            // div     07------>08
            // dis     08------>09
            // teh     10------>20
            NDVEntities db = new NDVEntities();
            var position = "";
            //zoom and color untill Province level
            if (args[0].ToString() != "null" && args[1].ToString() == "null" && args[2].ToString() == "null")
            {
                int provid = int.Parse(args[0].ToString());
                var prov = db.DbfToNDVs.First(p => (p.Dbf_ID == provid));
              //  int disid = int.Parse(args[1].ToString());
               // var dis = db.DbfToNDVs.First(d => (d.Dbf_ID == disid));
                //int subattid = int.Parse(args[4].ToString());
               // var teh = args[2].ToString();

               // string attrName = args[3].ToString();

                List<String> provArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select NAME_1 from PAK_adm1 where NAME_1 ='" + prov.Dbf_Loc_Name + "'";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm1.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    provArray.Add(row["NAME_1"].ToString());
                }
               
                InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer3"];
                mapShapeLayer.IsVisible = false;
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers["CapitalLayer"]);
                shapeFileLayer.Open();
                Collection<Feature> features = shapeFileLayer.QueryTools.GetFeaturesByColumnValue("NAME_1", prov.Dbf_Loc_Name, ReturningColumnsType.AllColumns);
                shapeFileLayer.ZoomLevelSet.ZoomLevel07.CustomStyles.Clear();
                
                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, provArray.Count);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_1";
                for (int i = 0; i < provArray.Count; i++)
                {
                    valueStyle.ValueItems.Add(new ValueItem(provArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[i]))));

                }



                shapeFileLayer.ZoomLevelSet.ZoomLevel07.CustomStyles.Add(valueStyle);
                shapeFileLayer.ZoomLevelSet.ZoomLevel07.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level08;
                shapeFileLayer.Close();
                position = features[0].GetBoundingBox().GetCenterPoint().ToString();

            }
            //zoom and color untill district level
            if (args[0].ToString() != "null" && args[1].ToString() != "null" && args[2].ToString() == "null")
            {
                int provid = int.Parse(args[0].ToString());
                var prov = db.DbfToNDVs.First(p => (p.Dbf_ID == provid));
                int disid = int.Parse(args[1].ToString());
                var dis = db.DbfToNDVs.First(d => (d.Dbf_ID == disid));
                //int subattid = int.Parse(args[4].ToString());
                //var teh = args[2].ToString();

              //  string attrName = args[3].ToString();

                List<String> distArray = new List<String>();

                string sql = null;
                DataTable dataTable = null;

                sql = "select NAME_3 from PAK_adm3 where NAME_3 ='" + dis.Dbf_Loc_Name + "'";

                ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/PAK_adm3.shp"));
                featureSource.Open();

                dataTable = featureSource.ExecuteQuery(sql);
                featureSource.Close();

                foreach (DataRow row in dataTable.Rows)
                {
                    distArray.Add(row["NAME_3"].ToString());
                }

                InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer2"];
                mapShapeLayer.IsVisible = false;
                LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
                ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers["DistrictLayer"]);
                shapeFileLayer.Open();
                Collection<Feature> features = shapeFileLayer.QueryTools.GetFeaturesByColumnValue("NAME_3", dis.Dbf_Loc_Name, ReturningColumnsType.AllColumns);
                shapeFileLayer.ZoomLevelSet.ZoomLevel10.CustomStyles.Clear();
                Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, distArray.Count);

                ValueStyle valueStyle = new ValueStyle();
                valueStyle.ColumnName = "NAME_3";
                for (int i = 0; i < distArray.Count; i++)
                {
                    valueStyle.ValueItems.Add(new ValueItem(distArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[i]))));

                }

                

                shapeFileLayer.ZoomLevelSet.ZoomLevel10.CustomStyles.Add(valueStyle);
                shapeFileLayer.ZoomLevelSet.ZoomLevel10.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level11;
                shapeFileLayer.Close();
                position = features[0].GetBoundingBox().GetCenterPoint().ToString();

            }
            //zoom and color untill tehsil lavel
            if (args[0].ToString() != "null" && args[1].ToString() != "null" && args[2].ToString() != "null") 
            { 
            int provid = int.Parse(args[0].ToString());
            var prov = db.DbfToNDVs.First(p => (p.Dbf_ID == provid));
            int disid = int.Parse(args[1].ToString());
            var dis = db.DbfToNDVs.First(d => (d.Dbf_ID == disid));
            //int subattid = int.Parse(args[4].ToString());
            var teh = args[2].ToString();
            
           // string attrName = args[3].ToString();

            List<String> tehsilArray = new List<String>();

            string sql = null;
            DataTable dataTable = null;

            sql = "select TEHSIL from Pak_Tehsil_Boundary where TEHSIL ='" + args[2].ToString() + "'";

            ShapeFileFeatureSource featureSource = new ShapeFileFeatureSource(Server.MapPath(@"~/ShapeFiles/Pak_Tehsil_Boundary.shp"));
            featureSource.Open();

            dataTable = featureSource.ExecuteQuery(sql);
            featureSource.Close();

            foreach (DataRow row in dataTable.Rows)
            {
                tehsilArray.Add(row["TEHSIL"].ToString());
            }

            InMemoryFeatureLayer mapShapeLayer = (InMemoryFeatureLayer)((LayerOverlay)map.CustomOverlays["HightLightDynamicOverlay"]).Layers["HighLightLayer"];
            mapShapeLayer.IsVisible = false;
            LayerOverlay staticOverlay = (LayerOverlay)map.CustomOverlays["StaticOverlay"];
            ShapeFileFeatureLayer shapeFileLayer = (ShapeFileFeatureLayer)(staticOverlay.Layers["TahseelLayer"]);
            shapeFileLayer.Open();
            Collection<Feature> features = shapeFileLayer.QueryTools.GetFeaturesByColumnValue("TEHSIL", teh, ReturningColumnsType.AllColumns);
            shapeFileLayer.ZoomLevelSet.ZoomLevel11.CustomStyles.Clear();
            Collection<GeoColor> colorsInFamily = GeoColor.GetColorsInQualityFamily(GeoColor.StandardColors.GreenYellow, tehsilArray.Count);
            
            ValueStyle valueStyle = new ValueStyle();
            valueStyle.ColumnName = "TEHSIL";
            for (int i = 0; i < tehsilArray.Count; i++)
            {
                valueStyle.ValueItems.Add(new ValueItem(tehsilArray[i], new AreaStyle(new GeoSolidBrush(colorsInFamily[i]))));
               
            }
            
            int colorCount = tehsilArray.Count();

            shapeFileLayer.ZoomLevelSet.ZoomLevel11.CustomStyles.Add(valueStyle);
            shapeFileLayer.ZoomLevelSet.ZoomLevel11.ApplyUntilZoomLevel = ApplyUntilZoomLevel.Level20;
            shapeFileLayer.Close();
            position = features[0].GetBoundingBox().GetCenterPoint().ToString();
            
            }
            return this.Json(position, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult OnlyProvinceWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();
            DbfToNDV loc = db.DbfToNDVs.First(b=>(b.Dbf_ID==hlp.Prov_ID));
            var location_List = (from yy in db.Locations
                                 where (yy.Province_ID == loc.NDV_ID)
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < ListOfMultiValues.Count; k = k + colName.Length)
                {
                    sum += ListOfMultiValues.ElementAt(k);
                }
                SumOfValues.Add(sum);   // alag alag values
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = colName;        // Names
            NamesAndValues[1] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceAndDistrictWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();
            Models.DbfToNDV prov = db.DbfToNDVs.First(x => (x.Dbf_ID == hlp.Prov_ID));//here comes province
            Models.DbfToNDV dist = db.DbfToNDVs.First(x => (x.Dbf_ID == hlp.Dist_ID));//here comes province
            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == prov.NDV_ID) && (yy.District_ID == dist.NDV_ID))
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var element = location_List.ElementAt(i);

                var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                           select zz.Scenerio_ID).ToList();

                for (int j = 0; j < sc1.Count; j++)     // Multi Values
                {
                    var element1 = sc1.ElementAt(j);
                    var Records = (from x in db.Multi_Value
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records);
                }
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < l.Count; k = k + colName.Length)
                {
                    sum += l.ElementAt(k);
                }
                SumOfValues.Add(sum);
            }
            String[] Years;
            Years = new String[3];
            Years[0] = "2007";
            Years[1] = "2008";
            Years[2] = "2009";
            Object[] NamesAndValues;
            NamesAndValues = new Object[3];
            NamesAndValues[0] = Years;        // Names
            NamesAndValues[1] = colName;        // Names
            NamesAndValues[2] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProvinceDistrictTehsilWithAttribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));

            var colName = (from x in db.Sub_Attribute      // sub-attribute column names
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var element = location_List.ElementAt(i);

                var sc1 = (from zz in db.Scenerios      // 13 Male Records => 1
                           where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == element))
                           select zz.Scenerio_ID).ToList();

                for (int j = 0; j < sc1.Count; j++)     // Multi Values
                {
                    var element1 = sc1.ElementAt(j);
                    var Records = (from x in db.Multi_Value
                                   where x.Scenerio_ID == element1
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records);
                }
            }

            List<long> l = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array fir = AllRecords.ElementAt(m);
                for (int k = 0; k < fir.Length; k++)
                {
                    l.Add((long)fir.GetValue(k));   // List of all values
                }
            }

            List<long> SumOfValues = new List<long>();
            for (int n = 0; n < colName.Length; n++)  // sum of all sub attributes
            {
                long sum = 0;
                for (int k = n; k < l.Count; k = k + colName.Length)
                {
                    sum += l.ElementAt(k);
                }
                SumOfValues.Add(sum);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = colName;        // Names
            NamesAndValues[1] = SumOfValues;    // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where (yy.Province_ID == hlp.Prov_ID)
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;
            long sum = 0;
            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            // sub ki id
            for (int k = (hlp.SubAttr - 1); k < ListOfMultiValues.Count; k = k + colName.Length)
            {
                sum += ListOfMultiValues.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = SubAttName;     // Names
            NamesAndValues[1] = sum;            // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult OnlyProvinceAndDistrictWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID))
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;
            long sum = 0;
            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            // sub ki id
            for (int k = hlp.SubAttr - 1; k < ListOfMultiValues.Count; k = k + colName.Length)
            {
                sum += ListOfMultiValues.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = SubAttName;     // Names
            NamesAndValues[1] = sum;            // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ProvinceDistrictTehsilWithSub_Attribute(Models.helping hlp)
        {
            NDVEntities db = new NDVEntities();

            var location_List = (from yy in db.Locations
                                 where ((yy.Province_ID == hlp.Prov_ID) && (yy.District_ID == hlp.Dist_ID) && (yy.Tehsil_ID == hlp.Teh_ID))
                                 select yy.Location_ID).ToList();

            Models.Attribute attr = db.Attributes.First(y => (y.Attribute_Name == hlp.Attr_Name));
            Models.Sub_Attribute sub_attr = db.Sub_Attribute.First(y => (y.SubAttr_ID == hlp.SubAttr));

            var colName = (from x in db.Sub_Attribute
                           where x.Attribute_ID == attr.Attribute_ID
                           select x.SubAttr_Name).ToArray();

            var SubAttName = from x in db.Sub_Attribute
                             where (x.Attribute_ID == attr.Attribute_ID && x.SubAttr_ID == hlp.SubAttr)
                             select x.SubAttr_Name;
            long sum = 0;
            List<Array> AllRecords = new List<Array>();
            for (int i = 0; i < location_List.Count; i++)
            {
                var Location_Ids = location_List.ElementAt(i); //20

                var Scenerio_List = (from zz in db.Scenerios        // 13
                                     where ((zz.Attribute_ID == attr.Attribute_ID) && (zz.Gender.Equals("Male")) && (zz.Location_ID == Location_Ids))
                                     select zz.Scenerio_ID).ToList();

                for (int j = 0; j < Scenerio_List.Count; j++)       // Multi Values
                {
                    var element1 = Scenerio_List.ElementAt(j);
                    var Records = (from x in db.Multi_Value     // 13
                                   where (x.Scenerio_ID == element1)
                                   select x.M_Attr_Value).ToArray(); // values ka sum krna hai ab kisi tarhaan bhi

                    AllRecords.Add(Records); // ( 13 * 4 ) * 20
                }
            }

            List<long> ListOfMultiValues = new List<long>();

            for (int m = 0; m < AllRecords.Count; m++)
            {
                Array Scenerio_Record = AllRecords.ElementAt(m);  // Group of 4 values
                for (int k = 0; k < Scenerio_Record.Length; k++)
                {
                    ListOfMultiValues.Add((long)Scenerio_Record.GetValue(k));   // List of all values -- sara kuch isi mai
                }
            }

            // sub ki id
            for (int k = hlp.SubAttr - 1; k < ListOfMultiValues.Count; k = k + colName.Length)
            {
                sum += ListOfMultiValues.ElementAt(k);
            }

            Object[] NamesAndValues;
            NamesAndValues = new Object[2];

            NamesAndValues[0] = SubAttName;     // Names
            NamesAndValues[1] = sum;            // Values

            return this.Json(NamesAndValues, JsonRequestBehavior.AllowGet);
        }
    }
}
