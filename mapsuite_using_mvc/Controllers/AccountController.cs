﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MapSuite_using_MVC.Models;

namespace MapSuite_using_MVC.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult UserProfile()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }

    }
}
