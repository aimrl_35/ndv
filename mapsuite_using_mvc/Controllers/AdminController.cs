﻿using MapSuite_using_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MapSuite_using_MVC.Controllers
{
    public class AdminController : Controller
    {
        public int AdmId = 0;
        public ActionResult AdminLogin()
        {
            Session["AdmMailID"] = "";
            Session["AdmUN"] = "";
            Session["AdmID"] = "";

            return View();
        }

        [HttpPost]
        public ActionResult LoginToDashboard()
        {
            NDVEntities db = new NDVEntities();

            var admin_mail = Request["adm_email"];
            var admin_pasword = Request["adm_password"];

           User_Info adm = db.User_Info.First(z => (z.Email.Equals(admin_mail)) && (z.Password.Equals(admin_pasword)));

                if ((adm.Email.Equals("hamxxa07@gmail.com")&&(adm.Password.Equals("123"))))
                {
                    Session["AdmMailID"] = adm.Email;
                    Session["AdmUN"] = adm.First_Name+" "+adm.Last_Name;
                    Session["AdmID"] = adm.User_ID;
                    return RedirectToAction("AdminDashboard");
                }
                else
                {
                    return RedirectToAction("AdminLogin");
                }
            
        }

        public ActionResult AdminDashboard()
        {
            AdmId = (int)Session["AdmID"];
            ViewBag.AdmName = Session["AdmUN"];
            return View();
        }
        [HttpPost]
        public JsonResult getUsers()
        {
            NDVEntities db = new NDVEntities();
            var userList = (from x in db.User_Info
                            select new { User_ID = x.User_ID, Phone_No = x.Phone_No, First_Name = x.First_Name, Last_Name = x.Last_Name, Job = x.Job, Gender = x.Gender, DOB = x.DOB, Email = x.Email, x = x.CNIC_No }).ToList();
            return this.Json(userList, JsonRequestBehavior.AllowGet);
        }

        public ActionResult deleteUser(int Id)
        {
            NDVEntities db = new NDVEntities();
            User_Info i = db.User_Info.Find(Id);
            db.User_Info.Remove(i);
            db.SaveChanges();

            return RedirectToAction("AdminDashboard");
        }
        [HttpPost]
        public JsonResult getMessages()
        {
            NDVEntities db = new NDVEntities();
            var messageList = (from x in db.UserMessages
                               select new { Id = x.Id, Username = x.Username, Email = x.Email, Message = x.Message, PhoneNo = x.PhoneNo }).ToList();
            return this.Json(messageList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult deleteMessage(int Id)
        {
            NDVEntities db = new NDVEntities();
            UserMessage i = db.UserMessages.Find(Id);
            db.UserMessages.Remove(i);
            db.SaveChanges();

            return RedirectToAction("AdminDashboard");
        }
        public ViewResult Posts()
        {
            return View();
        }
        public ViewResult ReportedPosts()
        {
            return View();
        }
        public ViewResult BlockedUsers()
        {
            return View();
        }

    }
}
